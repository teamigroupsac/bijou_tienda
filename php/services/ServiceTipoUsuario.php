<?php
ini_set('max_execution_time', 0);
require_once(INCDIR.'uc_functions.php');
require_once(INCDIR.'ez_sql/ez_sql_core.php');
require_once(INCDIR.'ez_sql/ez_sql_mysql.php');

require_once("Service.php");

class ServiceTipoUsuario extends Service
{
	
	function __construct() 
	{
		parent::__construct();
	}


	function getListaTipoUsuario(){
		$sql = "SELECT * FROM tipousuario ORDER BY idTipoUsuario ASC";
		$res = $this->db->get_results($sql);
		$this->_codificarObjeto($res,array("descripcionTipoUsuario"));
		return $res;
	}

	function getListaModulos(){
		$sql = "SELECT idPerfil,titulo FROM permisos";
		$res = $this->db->get_results($sql);
		return $res;
	}


	function saveFormularioTipoUsuario($data){
		$procedimiento = $data->procedimiento;
		$idTipoUsuario = $data->idTipoUsuario;
		$tipoUsuario = $data->tipoUsuario;
		//$permisos = $data->permisos;
		$usuario = $data->usuario;

		if($procedimiento == "GUARDAR"){
			$sql="INSERT INTO tipoUsuario (descripcionTipoUsuario)
				VALUES (UPPER('$tipoUsuario'))";

			$resNuevo=$this->db->query($sql);
		}else if($procedimiento == "MODIFICAR"){
			$sql="UPDATE tipoUsuario SET 
				descripcionTipoUsuario = UPPER('$tipoUsuario')
				WHERE idTipoUsuario = $idTipoUsuario";

			$resEditado=$this->db->query($sql);
		}

		if($resNuevo){
			return 1;
		}elseif($resEditado){
			return 2;
		}else{
			return 0;
		}

	}

    function deleteFormularioTipoUsuario($dato){
        $sql="DELETE FROM tipoUsuario WHERE idTipoUsuario = $dato";
        $res=$this->db->query($sql);
        

        if($res){
            return 1;
        }else{
            return 0;
        }

    }

    function eliminarMasivoFormularioTipoUsuario($data){
    	$usuarioRegistrador = $data->usuario;
    	$tipos = $data->tipos;

    	$listatipoUsuario = implode(",", $tipos);

        $sql="DELETE FROM tipoUsuario WHERE idTipoUsuario IN ($listatipoUsuario)";

        $res=$this->db->query($sql);
        

        if($res){
            return 1;
        }else{
            return 0;
        }

    }


    function modificarModuloPerfil($permisos,$tipousuario){
    	$sql="UPDATE tipousuario SET permisos = '$permisos' WHERE idTipoUsuario = $tipousuario";
    	$res=$this->db->query($sql);
    	return $tipousuario;
    }




}	
?>