<?php
ini_set('max_execution_time', 0);
require_once(INCDIR.'uc_functions.php');
require_once(INCDIR.'ez_sql/ez_sql_core.php');
require_once(INCDIR.'ez_sql/ez_sql_mysql.php');
require_once(INCDIR.'phpexcel/PHPExcel.php');
require_once("Service.php");

class ServiceGenerarCierre extends Service
{
	
	function __construct() 
	{
		parent::__construct();
	}

/*
	function generarCierre(){

		$sql_consulta = "SELECT COUNT(*) AS cuenta FROM captura WHERE sku_cap = ''";
		$res_consulta = $this->db->get_var($sql_consulta);

		if($res_consulta == 0){
			//LIMPIAR BASES TEMPORALES
			$sql="TRUNCATE capturas";
			$res=$this->db->query($sql);
			$sql="TRUNCATE diferencias";
			$res=$this->db->query($sql);

			//GENERAR TABLA CAPTURAS
			$sql="	INSERT INTO capturas
					SELECT 
					A.id_captura,A.area_cap,A.barra_cap,
					B.sku_barra AS sku_cap,
					SUM(A.cant_cap) AS cant_cap,
					A.tip_cap,A.usuario,A.fecha,A.hora,
					B.jerar,B.des_jerar,B.sub_dep_mst,B.des_sub_dep_mst,B.clas_mst,B.des_clas_mst,B.sub_clas_mst,B.des_sub_clas_mst,
					B.Prec_barra,B.des_barra
					FROM captura A LEFT JOIN maestro B
					ON A.barra_cap = B.cod_barra
					GROUP BY B.sku_barra";
			$res=$this->db->query($sql);

			$sql="	INSERT INTO diferencias
				
					SELECT s.sku_stk, s.des_sku_stk, ROUND( s.cost_prom_stk, 3 ) AS COSTO, 
					IFNULL( ROUND( s.cant_cer_stk,3 ) , 0 ) AS stock,
					IFNULL(SUM(ROUND(c.cant_cap,3)), 0)  AS contado, 
					ROUND((IFNULL(SUM(c.cant_cap),0) - IFNULL(s.cant_cer_stk,0)),3) AS dif_cant, 
					ROUND(IFNULL(s.cant_cer_stk * s.cost_prom_stk,0),3) AS total_stock_sol, 
					ROUND(IFNULL((SUM(c.cant_cap) * s.cost_prom_stk),0),3) AS total_cap_sol, 
					ROUND((IFNULL((SUM(c.cant_cap) * s.cost_prom_stk),0) - (s.cant_cer_stk * s.cost_prom_stk)),3) AS dif_sol,
					IF(ROUND((IFNULL((SUM(c.cant_cap) * s.cost_prom_stk),0) - (s.cant_cer_stk * s.cost_prom_stk)),3) < 0, ROUND((IFNULL((SUM(c.cant_cap) * s.cost_prom_stk),0) - (s.cant_cer_stk * s.cost_prom_stk)),3) * (-1),ROUND((IFNULL((SUM(c.cant_cap) * s.cost_prom_stk),0) - (s.cant_cer_stk * s.cost_prom_stk)),3)) AS dif_sol_pos,
					s.dep_stk,s.des_dep_stk,s.sub_dep_stk,s.des_sub_dep_stk,s.clas_stk,s.des_clas_stk,c.barra_cap
					FROM capturas c RIGHT JOIN stock_total s ON s.sku_stk=c.sku_cap
					GROUP BY s.sku_stk

					UNION DISTINCT

					SELECT c.sku_cap, c.des_barra, ROUND(c.precio_barra,3) AS COSTO, 
					ROUND(IFNULL(s.cant_cer_stk,0),3) AS stock, 
					ROUND(IFNULL(SUM(c.cant_cap),0),3)  AS contado,
					ROUND(IFNULL(SUM(c.cant_cap),0) - IFNULL(s.cant_cer_stk,0),3) AS dif_cant, 
					ROUND(IFNULL(s.cant_cer_stk * c.precio_barra,0),3) AS total_stock_sol, 
					ROUND(IFNULL((SUM(c.cant_cap) * c.precio_barra),0),3) AS total_cap_sol,
					ROUND(IFNULL(IFNULL((SUM(c.cant_cap) * c.precio_barra),0) - (IFNULL(s.cant_cer_stk,0) * c.precio_barra),0),3) AS dif_sol,
					IF(ROUND(IFNULL(IFNULL((SUM(c.cant_cap) * c.precio_barra),0) - (IFNULL(s.cant_cer_stk,0) * c.precio_barra),0),3) < 0,ROUND(IFNULL(IFNULL((SUM(c.cant_cap) * c.precio_barra),0) - (IFNULL(s.cant_cer_stk,0) * c.precio_barra),0),3) * (-1),ROUND(IFNULL(IFNULL((SUM(c.cant_cap) * c.precio_barra),0) - (IFNULL(s.cant_cer_stk,0) * c.precio_barra),0),3)) AS dif_sol_pos, 
					c.jerar, c.des_jerar,c.sub_dep_mst,c.des_sub_dep_mst,c.clas_mst,c.des_clas_mst,c.barra_cap
					FROM capturas c LEFT JOIN stock_total s ON c.sku_cap=s.sku_stk where s.sku_stk is null
					GROUP BY c.sku_cap
					";
			$res=$this->db->query($sql);

			return 1;			
		}else{
			return 0;
		}

	}
*/
	
	function getGraficoDiferenciaJerarquica(){
		$sqlJer="	SELECT CONCAT(linea,' ( ',IF(ROUND(SUM(dif_sol),2)<0,'-','+'),' )') AS jerarquias
					FROM diferencias
					GROUP BY linea
					ORDER BY ABS(SUM(dif_sol)) DESC";
		$resJer = $this->db->get_results($sqlJer);
		$this->_codificarObjeto($resJer,array("jerarquias"));

		$sqlVal="	SELECT 
					IF(ROUND(SUM(dif_sol),3)<0,ROUND(SUM(dif_sol),2)*(-1),ROUND(SUM(dif_sol),2)) AS diferencia_soles,
					ROUND(SUM(dif_sol),2) AS diferencia
					FROM diferencias
					GROUP BY linea
					ORDER BY ABS(SUM(dif_sol)) DESC";
		$resVal = $this->db->get_results($sqlVal);

		$sqlPor="	SELECT 
					ROUND(( IF(ROUND(SUM(dif_sol),3)<0,ROUND(SUM(dif_sol),2)*(-1),ROUND(SUM(dif_sol),2)) / ( SELECT IF(ROUND(SUM(dif_sol),3)<0,ROUND(SUM(dif_sol),3)*(-1),ROUND(SUM(dif_sol),3)) AS diferencia_soles FROM diferencias ) * 100),2) AS porcentajes
					FROM diferencias
					GROUP BY linea
					ORDER BY ABS(SUM(dif_sol)) DESC";
		$resPor = $this->db->get_results($sqlPor);

		$valores = new stdClass();
        $valores->rangos = $resJer;
        $valores->valores = $resVal;
        $valores->porcentajes = $resPor;
        $valores->porcentaje = $this->getPorcentajeAvanceTotal();

		return $valores;
	}


	function getDescargaReporteDiferenciaJerarquica(){
		$hora = date("h:i:s");
    	$fecha = date("Y-m-j");

    	$total_unit_cli = 0.000;
    	$total_soles_cli = 0.000;
    	$total_unit_igroup = 0.000;
    	$total_soles_igroup = 0.000;
    	$total_dif_unit = 0.000;
    	$total_dif_sol = 0.000;


		$sql="	SELECT CONCAT(TRIM(dep_stk),' - ',des_dep_stk) AS jerarquias,
				ROUND(SUM(stock),3) AS unidades_cliente, ROUND(SUM(total_stock_sol),3) AS soles_cliente,
				ROUND(SUM(contado),3) AS unidades_igroup, ROUND(SUM(total_cap_sol),3) soles_igroup,
				ROUND((SUM(contado) - SUM(stock)),3) AS diferencia_unidades,
				ROUND((SUM(total_cap_sol) - SUM(total_stock_sol)),3) AS diferencia_soles FROM diferencias
				GROUP BY dep_stk";
		$res = $this->db->get_results($sql);
		$this->_codificarObjeto($res,array("jerarquias"));

		if($res){
							
			date_default_timezone_set('America/Mexico_City');

			if (PHP_SAPI == 'cli')
				die('Este archivo solo se puede ver desde un navegador web');

			/** Se agrega la libreria PHPExcel */
			//require_once 'lib/PHPExcel/PHPExcel.php';

			// Se crea el objeto PHPExcel
			$objPHPExcel = new PHPExcel();

			// Se asignan las propiedades del libro
			$objPHPExcel->getProperties()->setCreator("CSPSISTEMAS") //Autor
								 ->setLastModifiedBy("CSPSISTEMAS") //Ultimo usuario que lo modificó
								 ->setTitle("Reporte Excel con PHP y MySQL")
								 ->setSubject("Reporte Excel con PHP y MySQL")
								 ->setDescription("Reporte de alumnos")
								 ->setKeywords("reporte alumnos carreras")
								 ->setCategory("Reporte excel");

			$tituloReporte = "REPORTE DE DIFERENCIAS POR JERARQUIA";
			$titulosColumnas = array('N','JERARQUIAS','UNIT CLI','SOLES CLI','UNI IGROUP','SOLES IGROUP','DIF UNIT','DIF SOL');
			
			$objPHPExcel->setActiveSheetIndex(0)
	        		    ->mergeCells('A1:H1');
							
			// Se agregan los titulos del reporte
			$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue('A1',$tituloReporte)
						->setCellValue('G1','FECHA : ')
						->setCellValue('H1',$fecha)
						->setCellValue('G2','HORA : ')
						->setCellValue('H2',$hora)
	        		    ->setCellValue('A3',  $titulosColumnas[0])
			            ->setCellValue('B3',  $titulosColumnas[1])
	        		    ->setCellValue('C3',  $titulosColumnas[2])
	            		->setCellValue('D3',  $titulosColumnas[3])
	            		->setCellValue('E3',  $titulosColumnas[4])
	            		->setCellValue('F3',  $titulosColumnas[5])
	            		->setCellValue('G3',  $titulosColumnas[6])
	            		->setCellValue('H3',  $titulosColumnas[7]);
			
			//Se agregan los datos de los alumnos
			$i = 4;

			for( $x = 0; $x < count($res); $x++)
			{

		    	$total_unit_cli = $total_unit_cli + $res[$x]->unidades_cliente;
		    	$total_soles_cli = $total_soles_cli + $res[$x]->soles_cliente;
		    	$total_unit_igroup = $total_unit_igroup + $res[$x]->unidades_igroup;
		    	$total_soles_igroup = $total_soles_igroup + $res[$x]->soles_igroup;
		    	$total_dif_unit = $total_dif_unit + $res[$x]->diferencia_unidades;
		    	$total_dif_sol = $total_dif_sol + $res[$x]->diferencia_soles;

				$objPHPExcel->setActiveSheetIndex(0)
	        		    ->setCellValue('A'.$i,  ($x + 1))
			            ->setCellValue('B'.$i,  $res[$x]->jerarquias)
	        		    ->setCellValue('C'.$i,  $res[$x]->unidades_cliente)
	        		    ->setCellValue('D'.$i,  $res[$x]->soles_cliente)
	        		    ->setCellValue('E'.$i,  $res[$x]->unidades_igroup)
	        		    ->setCellValue('F'.$i,  $res[$x]->soles_igroup)
	        		    ->setCellValue('G'.$i,  $res[$x]->diferencia_unidades)
	        		    ->setCellValue('H'.$i,  $res[$x]->diferencia_soles);
						$i++;
			}

				$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue('B'.$i,  "TOTALES")
	        		    ->setCellValue('C'.$i,  $total_unit_cli)
	        		    ->setCellValue('D'.$i,  $total_soles_cli)
	        		    ->setCellValue('E'.$i,  $total_unit_igroup)
	        		    ->setCellValue('F'.$i,  $total_soles_igroup)
	        		    ->setCellValue('G'.$i,  $total_dif_unit)
	        		    ->setCellValue('H'.$i,  $total_dif_sol);


			
			$estiloTituloReporte = array(
	        	'font' => array(
		        	'name'      => 'Verdana',
	    	        'bold'      => true,
	        	    'italic'    => false,
	                'strike'    => false,
	               	'size' =>16,
		            	'color'     => array(
	    	            	'rgb' => 'FFFFFF'
	        	       	)
	            ),
		        'fill' => array(
					'type'	=> PHPExcel_Style_Fill::FILL_SOLID,
					'color'	=> array('argb' => 'FF220835')
				),
	            'borders' => array(
	               	'allborders' => array(
	                	'style' => PHPExcel_Style_Border::BORDER_NONE                    
	               	)
	            ), 
	            'alignment' =>  array(
	        			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
	        			'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
	        			'rotation'   => 0,
	        			'wrap'          => TRUE
	    		)
	        );

			$estiloTituloColumnas = array(
	            'font' => array(
	                'name'      => 'Arial',
	                'bold'      => true,                          
	                'color'     => array(
	                    'rgb' => 'FFFFFF'
	                )
	            ),
	            'fill' 	=> array(
					'type'		=> PHPExcel_Style_Fill::FILL_GRADIENT_LINEAR,
					'rotation'   => 90,
	        		'startcolor' => array(
	            		'rgb' => 'c47cf2'
	        		),
	        		'endcolor'   => array(
	            		'argb' => 'FF431a5d'
	        		)
				),
	            'borders' => array(
	            	'top'     => array(
	                    'style' => PHPExcel_Style_Border::BORDER_MEDIUM ,
	                    'color' => array(
	                        'rgb' => '143860'
	                    )
	                ),
	                'bottom'     => array(
	                    'style' => PHPExcel_Style_Border::BORDER_MEDIUM ,
	                    'color' => array(
	                        'rgb' => '143860'
	                    )
	                )
	            ),
				'alignment' =>  array(
	        			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
	        			'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
	        			'wrap'          => TRUE
	    		));
				
			$estiloInformacion = new PHPExcel_Style();
			$estiloInformacion->applyFromArray(
				array(
	           		'font' => array(
	               	'name'      => 'Arial',               
	               	'color'     => array(
	                   	'rgb' => '000000'
	               	)
	           	),
	           	'fill' 	=> array(
					'type'		=> PHPExcel_Style_Fill::FILL_SOLID,
					'color'		=> array('argb' => 'FFd9b7f4')
				),
	           	'borders' => array(
	               	'left'     => array(
	                   	'style' => PHPExcel_Style_Border::BORDER_THIN ,
		                'color' => array(
	    	            	'rgb' => '3a2a47'
	                   	)
	               	)             
	           	)
	        ));
			 
			//$objPHPExcel->getActiveSheet()->getStyle('A1:D1')->applyFromArray($estiloTituloReporte);
			//$objPHPExcel->getActiveSheet()->getStyle('A3:D3')->applyFromArray($estiloTituloColumnas);		
			//$objPHPExcel->getActiveSheet()->setSharedStyle($estiloInformacion, "A4:R".($i-1));
					
			for($i = 'A'; $i <= 'H'; $i++){
				$objPHPExcel->setActiveSheetIndex(0)			
					->getColumnDimension($i)->setAutoSize(TRUE);
			}
			
			// Se asigna el nombre a la hoja
			$objPHPExcel->getActiveSheet()->setTitle('Reporte');

			// Se activa la hoja para que sea la que se muestre cuando el archivo se abre
			$objPHPExcel->setActiveSheetIndex(0);
			// Inmovilizar paneles 
			//$objPHPExcel->getActiveSheet(0)->freezePane('A4');
			$objPHPExcel->getActiveSheet(0)->freezePaneByColumnAndRow(0,4);

			// Se manda el archivo al navegador web, con el nombre que se indica (Excel2007)
			header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			header('Content-Disposition: attachment;filename="reportediferenciajerarquica.xlsx"');
			header('Cache-Control: max-age=0');

			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
			$objWriter->save('reportes/reportediferenciajerarquica.xlsx');

			return "ok";
			
		}
		else{
			print_r('No hay resultados para mostrar');
		}
	}

	function getListaJerarquias(){
		$sql="	SELECT linea AS linea FROM diferencias
				GROUP BY linea
				ORDER BY linea ASC";
		$res = $this->db->get_results($sql);
		$this->_codificarObjeto($res,array("linea"));
		return $res;
	}

	function getListaSubJerarquias($jera){
		$sql="	SELECT sub_dep_stk, CONCAT(TRIM(sub_dep_stk),' - ',des_sub_dep_stk) AS subjerarquia FROM diferencias
				WHERE dep_stk = '$jera'
				GROUP BY sub_dep_stk
				ORDER BY sub_dep_stk ASC";
		$res = $this->db->get_results($sql);
		$this->_codificarObjeto($res,array("jerarquia"));
		return $res;
	}



	function getDescargaReporteDiferencia($monto,$jera,$subjera){
		$hora = date("h:i:s");
    	$fecha = date("Y-m-j");

		$jerarquia = "";
		if($jera != '0'){
			$jerarquia = " AND dep_stk = '".$jera."'";
			$tjerarquia = " - JERARQUIA [ ".TRIM($jera)." ]";
		}

		$subjerarquia = "";
		if($subjera != '0'){
			$subjerarquia = " AND sub_dep_stk = '".$subjera."'";
			$tsubjerarquia = " - SUB JERARQUIA [ ".TRIM($subjera)." ]";
		}

		$sql="	SELECT sku_stk, barra_cap, des_sku_stk, stock, contado, dif_cant, dif_sol FROM diferencias
				WHERE dif_sol_pos >= $monto $jerarquia $subjerarquia
				ORDER BY dif_sol_pos DESC";
		$res = $this->db->get_results($sql);
		$this->_codificarObjeto($res,array("des_sku_stk"));

		$sql_area_rango = "SELECT * FROM area_rango";
		$resAreaRango = $this->db->get_results($sql_area_rango);
		$this->_codificarObjeto($resAreaRango,array("des_area_ran"));

		if($res && $resAreaRango){
							
			date_default_timezone_set('America/Mexico_City');

			if (PHP_SAPI == 'cli')
				die('Este archivo solo se puede ver desde un navegador web');

			/** Se agrega la libreria PHPExcel */
			//require_once 'lib/PHPExcel/PHPExcel.php';

			// Se crea el objeto PHPExcel
			$objPHPExcel = new PHPExcel();

			// Se asignan las propiedades del libro
			$objPHPExcel->getProperties()->setCreator("CSPSISTEMAS") //Autor
								 ->setLastModifiedBy("CSPSISTEMAS") //Ultimo usuario que lo modificó
								 ->setTitle("Reporte Excel con PHP y MySQL")
								 ->setSubject("Reporte Excel con PHP y MySQL")
								 ->setDescription("Reporte de alumnos")
								 ->setKeywords("reporte alumnos carreras")
								 ->setCategory("Reporte excel");


			$tituloReporte = "REPORTE DE DIFERENCIAS - MONTO FIJADO S/. ".number_format($monto,0)."".$tjerarquia."".$tsubjerarquia;
			$titulosColumnas = array('N','SKU','BARRA','DESCRIPCION','STOCK','CONTEO','DIF UNIT','DIF SOL');
			
			$objPHPExcel->setActiveSheetIndex(0)
	        		    ->mergeCells('A1:H1');
							
			// Se agregan los titulos del reporte
			$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue('A1',$tituloReporte)
						->setCellValue('G1','FECHA : ')
						->setCellValue('H1',$fecha)
						->setCellValue('G2','HORA : ')
						->setCellValue('H2',$hora)
	        		    ->setCellValue('A3',  $titulosColumnas[0])
			            ->setCellValue('B3',  $titulosColumnas[1])
	        		    ->setCellValue('C3',  $titulosColumnas[2])
	            		->setCellValue('D3',  $titulosColumnas[3])
	            		->setCellValue('E3',  $titulosColumnas[4])
	            		->setCellValue('F3',  $titulosColumnas[5])
	            		->setCellValue('G3',  $titulosColumnas[6])
	            		->setCellValue('H3',  $titulosColumnas[7]);
			
			//Se agregan los datos de los alumnos
			$i = 4;

			for( $x = 0; $x < count($res); $x++)
			{
				$objPHPExcel->setActiveSheetIndex(0)
	        		    ->setCellValue('A'.$i,  ($x + 1))
			            ->setCellValue('B'.$i,  $res[$x]->sku_stk)
	        		    ->setCellValue('C'.$i,  $res[$x]->barra_cap)
	        		    ->setCellValue('D'.$i,  $res[$x]->des_sku_stk)
	        		    ->setCellValue('E'.$i,  number_format($res[$x]->stock,0))
	        		    ->setCellValue('F'.$i,  number_format($res[$x]->contado,0))
	        		    ->setCellValue('G'.$i,  number_format($res[$x]->dif_cant,0))
	        		    ->setCellValue('H'.$i,  number_format($res[$x]->dif_sol,0));
						$i++;

			            $barra = $res[$x]->barra_cap;
						$sqlDetalle="	SELECT barra_cap, area_cap, cant_cap FROM captura
								WHERE barra_cap = '$barra'
								ORDER BY area_cap ASC";
						$resDetalle = $this->db->get_results($sqlDetalle);
			            $detalle = $resDetalle;

			            if(count($detalle) > 0){
			                foreach ($detalle as $lote) {
			                    $detallearea = "";
			                    //AQUI LA CONSULTA CON LA UBICACION DEL LOTE
			                    foreach ($resAreaRango as $arearango) {
			                        if( ( (float)$arearango->area_ini_ran <= (float)$lote->area_cap ) AND ( (float)$arearango->area_fin_ran >= (float)$lote->area_cap ) ){
			                            $detallearea =  $arearango->des_area_ran;
			                        }
			                    }

								$objPHPExcel->setActiveSheetIndex(0)
					        		    ->setCellValue('C'.$i,  $lote->barra_cap)
					        		    ->setCellValue('D'.$i,  'Lote : '.$lote->area_cap.' ==> '.$detallearea)
					        		    ->setCellValue('E'.$i,  'Contado : ')
					        		    ->setCellValue('F'.$i,  number_format($lote->cant_cap,0));
										$i++;
			                }

			            }


			}

			
			$estiloTituloReporte = array(
	        	'font' => array(
		        	'name'      => 'Verdana',
	    	        'bold'      => true,
	        	    'italic'    => false,
	                'strike'    => false,
	               	'size' =>16,
		            	'color'     => array(
	    	            	'rgb' => 'FFFFFF'
	        	       	)
	            ),
		        'fill' => array(
					'type'	=> PHPExcel_Style_Fill::FILL_SOLID,
					'color'	=> array('argb' => 'FF220835')
				),
	            'borders' => array(
	               	'allborders' => array(
	                	'style' => PHPExcel_Style_Border::BORDER_NONE                    
	               	)
	            ), 
	            'alignment' =>  array(
	        			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
	        			'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
	        			'rotation'   => 0,
	        			'wrap'          => TRUE
	    		)
	        );

			$estiloTituloColumnas = array(
	            'font' => array(
	                'name'      => 'Arial',
	                'bold'      => true,                          
	                'color'     => array(
	                    'rgb' => 'FFFFFF'
	                )
	            ),
	            'fill' 	=> array(
					'type'		=> PHPExcel_Style_Fill::FILL_GRADIENT_LINEAR,
					'rotation'   => 90,
	        		'startcolor' => array(
	            		'rgb' => 'c47cf2'
	        		),
	        		'endcolor'   => array(
	            		'argb' => 'FF431a5d'
	        		)
				),
	            'borders' => array(
	            	'top'     => array(
	                    'style' => PHPExcel_Style_Border::BORDER_MEDIUM ,
	                    'color' => array(
	                        'rgb' => '143860'
	                    )
	                ),
	                'bottom'     => array(
	                    'style' => PHPExcel_Style_Border::BORDER_MEDIUM ,
	                    'color' => array(
	                        'rgb' => '143860'
	                    )
	                )
	            ),
				'alignment' =>  array(
	        			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
	        			'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
	        			'wrap'          => TRUE
	    		));
				
			$estiloInformacion = new PHPExcel_Style();
			$estiloInformacion->applyFromArray(
				array(
	           		'font' => array(
	               	'name'      => 'Arial',               
	               	'color'     => array(
	                   	'rgb' => '000000'
	               	)
	           	),
	           	'fill' 	=> array(
					'type'		=> PHPExcel_Style_Fill::FILL_SOLID,
					'color'		=> array('argb' => 'FFd9b7f4')
				),
	           	'borders' => array(
	               	'left'     => array(
	                   	'style' => PHPExcel_Style_Border::BORDER_THIN ,
		                'color' => array(
	    	            	'rgb' => '3a2a47'
	                   	)
	               	)             
	           	)
	        ));
			 
			//$objPHPExcel->getActiveSheet()->getStyle('A1:D1')->applyFromArray($estiloTituloReporte);
			//$objPHPExcel->getActiveSheet()->getStyle('A3:D3')->applyFromArray($estiloTituloColumnas);		
			//$objPHPExcel->getActiveSheet()->setSharedStyle($estiloInformacion, "A4:R".($i-1));
					
			for($i = 'A'; $i <= 'H'; $i++){
				$objPHPExcel->setActiveSheetIndex(0)			
					->getColumnDimension($i)->setAutoSize(TRUE);
			}
			
			// Se asigna el nombre a la hoja
			$objPHPExcel->getActiveSheet()->setTitle('Reporte');

			// Se activa la hoja para que sea la que se muestre cuando el archivo se abre
			$objPHPExcel->setActiveSheetIndex(0);
			// Inmovilizar paneles 
			//$objPHPExcel->getActiveSheet(0)->freezePane('A4');
			$objPHPExcel->getActiveSheet(0)->freezePaneByColumnAndRow(0,4);

			// Se manda el archivo al navegador web, con el nombre que se indica (Excel2007)
			header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			header('Content-Disposition: attachment;filename="reportediferencia.xlsx"');
			header('Cache-Control: max-age=0');

			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
			$objWriter->save('reportes/reportediferencia.xlsx');

			return "ok";
			
		}
		else{
			print_r('No hay resultados para mostrar');
		}
	}



	function getDescargaReporteDiferenciaSku($limite){
		$hora = date("h:i:s");
    	$fecha = date("Y-m-j");

		$sql="	SELECT * FROM diferencias
				ORDER BY dif_sol_pos DESC
				LIMIT $limite";
		$res = $this->db->get_results($sql);
		$this->_codificarObjeto($res,array("des_sku_stk"));

		$sql_area_rango = "SELECT * FROM area_rango";
		$resAreaRango = $this->db->get_results($sql_area_rango);
		$this->_codificarObjeto($resAreaRango,array("des_area_ran"));

		if($res && $resAreaRango){
							
			date_default_timezone_set('America/Mexico_City');

			if (PHP_SAPI == 'cli')
				die('Este archivo solo se puede ver desde un navegador web');

			/** Se agrega la libreria PHPExcel */
			//require_once 'lib/PHPExcel/PHPExcel.php';

			// Se crea el objeto PHPExcel
			$objPHPExcel = new PHPExcel();

			// Se asignan las propiedades del libro
			$objPHPExcel->getProperties()->setCreator("CSPSISTEMAS") //Autor
								 ->setLastModifiedBy("CSPSISTEMAS") //Ultimo usuario que lo modificó
								 ->setTitle("Reporte Excel con PHP y MySQL")
								 ->setSubject("Reporte Excel con PHP y MySQL")
								 ->setDescription("Reporte de alumnos")
								 ->setKeywords("reporte alumnos carreras")
								 ->setCategory("Reporte excel");


			$tituloReporte = "REPORTE DE DIFERENCIAS POR SKU - LIMITE ".$limite;
			$titulosColumnas = array('N','SKU','BARRA','DESCRIPCION','STOCK','CONTEO','DIF UNIT','DIF SOL');
			
			$objPHPExcel->setActiveSheetIndex(0)
	        		    ->mergeCells('A1:H1');
							
			// Se agregan los titulos del reporte
			$objPHPExcel->setActiveSheetIndex(0)
						->setCellValue('A1',$tituloReporte)
						->setCellValue('G1','FECHA : ')
						->setCellValue('H1',$fecha)
						->setCellValue('G2','HORA : ')
						->setCellValue('H2',$hora)
	        		    ->setCellValue('A3',  $titulosColumnas[0])
			            ->setCellValue('B3',  $titulosColumnas[1])
	        		    ->setCellValue('C3',  $titulosColumnas[2])
	            		->setCellValue('D3',  $titulosColumnas[3])
	            		->setCellValue('E3',  $titulosColumnas[4])
	            		->setCellValue('F3',  $titulosColumnas[5])
	            		->setCellValue('G3',  $titulosColumnas[6])
	            		->setCellValue('H3',  $titulosColumnas[7]);
			
			//Se agregan los datos de los alumnos
			$i = 4;

			for( $x = 0; $x < count($res); $x++)
			{
				$objPHPExcel->setActiveSheetIndex(0)
	        		    ->setCellValue('A'.$i,  ($x + 1))
			            ->setCellValue('B'.$i,  $res[$x]->sku_stk)
	        		    ->setCellValue('C'.$i,  $res[$x]->barra_cap)
	        		    ->setCellValue('D'.$i,  $res[$x]->des_sku_stk)
	        		    ->setCellValue('E'.$i,  number_format($res[$x]->stock,0))
	        		    ->setCellValue('F'.$i,  number_format($res[$x]->contado,0))
	        		    ->setCellValue('G'.$i,  number_format($res[$x]->dif_cant,0))
	        		    ->setCellValue('H'.$i,  number_format($res[$x]->dif_sol,0));
						$i++;

			            $barra = $res[$x]->barra_cap;
						$sqlDetalle="	SELECT barra_cap, area_cap, cant_cap FROM captura
								WHERE barra_cap = '$barra'
								ORDER BY area_cap ASC";
						$resDetalle = $this->db->get_results($sqlDetalle);
			            $detalle = $resDetalle;

			            if(count($detalle) > 0){
			                foreach ($detalle as $lote) {
			                    $detallearea = "";
			                    //AQUI LA CONSULTA CON LA UBICACION DEL LOTE
			                    foreach ($resAreaRango as $arearango) {
			                        if( ( (float)$arearango->area_ini_ran <= (float)$lote->area_cap ) AND ( (float)$arearango->area_fin_ran >= (float)$lote->area_cap ) ){
			                            $detallearea =  $arearango->des_area_ran;
			                        }
			                    }

								$objPHPExcel->setActiveSheetIndex(0)
					        		    ->setCellValue('C'.$i,  $lote->barra_cap)
					        		    ->setCellValue('D'.$i,  'Lote : '.$lote->area_cap.' ==> '.$detallearea)
					        		    ->setCellValue('E'.$i,  'Contado : ')
					        		    ->setCellValue('F'.$i,  number_format($lote->cant_cap,0));
										$i++;
			                }

			            }


			}

			
			$estiloTituloReporte = array(
	        	'font' => array(
		        	'name'      => 'Verdana',
	    	        'bold'      => true,
	        	    'italic'    => false,
	                'strike'    => false,
	               	'size' =>16,
		            	'color'     => array(
	    	            	'rgb' => 'FFFFFF'
	        	       	)
	            ),
		        'fill' => array(
					'type'	=> PHPExcel_Style_Fill::FILL_SOLID,
					'color'	=> array('argb' => 'FF220835')
				),
	            'borders' => array(
	               	'allborders' => array(
	                	'style' => PHPExcel_Style_Border::BORDER_NONE                    
	               	)
	            ), 
	            'alignment' =>  array(
	        			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
	        			'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
	        			'rotation'   => 0,
	        			'wrap'          => TRUE
	    		)
	        );

			$estiloTituloColumnas = array(
	            'font' => array(
	                'name'      => 'Arial',
	                'bold'      => true,                          
	                'color'     => array(
	                    'rgb' => 'FFFFFF'
	                )
	            ),
	            'fill' 	=> array(
					'type'		=> PHPExcel_Style_Fill::FILL_GRADIENT_LINEAR,
					'rotation'   => 90,
	        		'startcolor' => array(
	            		'rgb' => 'c47cf2'
	        		),
	        		'endcolor'   => array(
	            		'argb' => 'FF431a5d'
	        		)
				),
	            'borders' => array(
	            	'top'     => array(
	                    'style' => PHPExcel_Style_Border::BORDER_MEDIUM ,
	                    'color' => array(
	                        'rgb' => '143860'
	                    )
	                ),
	                'bottom'     => array(
	                    'style' => PHPExcel_Style_Border::BORDER_MEDIUM ,
	                    'color' => array(
	                        'rgb' => '143860'
	                    )
	                )
	            ),
				'alignment' =>  array(
	        			'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
	        			'vertical'   => PHPExcel_Style_Alignment::VERTICAL_CENTER,
	        			'wrap'          => TRUE
	    		));
				
			$estiloInformacion = new PHPExcel_Style();
			$estiloInformacion->applyFromArray(
				array(
	           		'font' => array(
	               	'name'      => 'Arial',               
	               	'color'     => array(
	                   	'rgb' => '000000'
	               	)
	           	),
	           	'fill' 	=> array(
					'type'		=> PHPExcel_Style_Fill::FILL_SOLID,
					'color'		=> array('argb' => 'FFd9b7f4')
				),
	           	'borders' => array(
	               	'left'     => array(
	                   	'style' => PHPExcel_Style_Border::BORDER_THIN ,
		                'color' => array(
	    	            	'rgb' => '3a2a47'
	                   	)
	               	)             
	           	)
	        ));
			 
			//$objPHPExcel->getActiveSheet()->getStyle('A1:D1')->applyFromArray($estiloTituloReporte);
			//$objPHPExcel->getActiveSheet()->getStyle('A3:D3')->applyFromArray($estiloTituloColumnas);		
			//$objPHPExcel->getActiveSheet()->setSharedStyle($estiloInformacion, "A4:R".($i-1));
					
			for($i = 'A'; $i <= 'H'; $i++){
				$objPHPExcel->setActiveSheetIndex(0)			
					->getColumnDimension($i)->setAutoSize(TRUE);
			}
			
			// Se asigna el nombre a la hoja
			$objPHPExcel->getActiveSheet()->setTitle('Reporte');

			// Se activa la hoja para que sea la que se muestre cuando el archivo se abre
			$objPHPExcel->setActiveSheetIndex(0);
			// Inmovilizar paneles 
			//$objPHPExcel->getActiveSheet(0)->freezePane('A4');
			$objPHPExcel->getActiveSheet(0)->freezePaneByColumnAndRow(0,4);

			// Se manda el archivo al navegador web, con el nombre que se indica (Excel2007)
			header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			header('Content-Disposition: attachment;filename="reportediferenciasku.xlsx"');
			header('Cache-Control: max-age=0');

			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
			$objWriter->save('reportes/reportediferenciasku.xlsx');

			return "ok";
			
		}
		else{
			print_r('No hay resultados para mostrar');
		}
	}


	function getRegistrosDiferenciaGrupos(){
		$sql="	SELECT SUBSTR(D.dep_stk,1,3) AS idGrupo, G.jerarquias, G.division 
				FROM diferencias D LEFT JOIN grupos G
				ON SUBSTR(D.dep_stk,1,3) = G.idGrupo
				GROUP BY SUBSTR(D.dep_stk,1,3)
				ORDER BY SUBSTR(D.dep_stk,1,3) ASC";
		$res = $this->db->get_results($sql);
		$this->_codificarObjeto($res,array("idGrupo","jerarquias","division"));
		return $res;
	}

	function saveGrupo($data){
		$idGrupo = $data->idGrupo;
		$jerarquias = $data->jerarquias;
		$division = $data->division;
		$sqlEliminar="DELETE FROM grupos WHERE idGrupo = '$idGrupo'";
		$resEliminar=$this->db->query($sqlEliminar);

		if($jerarquias != "" && $division != ""){
			$sqlGuardar = "	INSERT INTO grupos (idGrupo,jerarquias,division)
							VALUES ('$idGrupo','$jerarquias','$division')";
			$resGuardar=$this->db->query($sqlGuardar);
		}

		$resultado = new stdClass();
        $resultado->grupo = $idGrupo;

	    if($resGuardar){
	    	$resultado->estado = "OK";
	    }else{
	        $resultado->estado = "ERROR";
	    }

	    return $resultado;
	}

	function generarReporteProductividad($estado){
		$sql="	SELECT U.dniUsuario, U.nombreUsuario,
				(SELECT inicioAsistencia FROM asistencia WHERE dniUsuario = U.dniUsuario ORDER BY inicioAsistencia ASC LIMIT 1) inicioAsistencia, 
				(SELECT terminoAsistencia FROM asistencia WHERE dniUsuario = U.dniUsuario ORDER BY inicioAsistencia DESC LIMIT 1) terminoAsistencia,
				SUM((TIMESTAMPDIFF(SECOND , A.inicioAsistencia, A.terminoAsistencia ))/60/60) horas_conteo,
				(SELECT SUM(cant_cap) FROM captura WHERE usuario = U.dniUsuario) total_conteo,
				((SELECT SUM(cant_cap) FROM captura WHERE usuario = U.dniUsuario) / SUM((TIMESTAMPDIFF(SECOND , A.inicioAsistencia, A.terminoAsistencia ))/60/60)) conteo_x_hora 
				FROM usuario U LEFT JOIN asistencia A
				ON U.dniUsuario = A.dniUsuario
				WHERE U.estadoUsuario = $estado
				GROUP BY U.dniUsuario";
		$res = $this->db->get_results($sql);

		$archivo = "../archivos_sistema/archivos_generados/archivo_reporte_productividad.txt";
		unlink('$archivo');

		$conteo=1;
		$cadena.="\r\n";
		$cadena.="N|DNI|NOMBRE COMPLETO|INICIO CONTEO|FIN CONTEO|HORAS CONTEO|TOTAL CONTADO|CONTEO X HORA";
		for($i=0;$i<count($res);$i++){
			$conteo++;
			$cadena.="\r\n";
			$cadena.= $conteo."|".$res[$i]->dniUsuario."|".$res[$i]->nombreUsuario."|".$res[$i]->inicioAsistencia."|".$res[$i]->terminoAsistencia."|".$res[$i]->horas_conteo."|".$res[$i]->total_conteo."|".$res[$i]->conteo_x_hora;
        }

		$cadenax = substr($cadena,1,strlen($cadena));
		$fch= fopen($archivo, "w"); // Abres el archivo para escribir en él
		fwrite($fch, $cadenax); // Grabas
		fclose($fch); // Cierras el archivo.


        $bytes = filesize($archivo);
        $label = array( 'B', 'KB', 'MB', 'GB', 'TB', 'PB' );
        for( $i = 0; $bytes >= 1024 && $i < ( count( $label ) -1 ); $bytes /= 1024, $i++ );
        $peso = ( round( $bytes, 2 ) . " " . $label[$i] );

        $file = new stdClass();
        $file->nombre = $archivo;
        $file->filas = $conteo;
        $file->peso = $peso;
        $file->fecha = date("Y-m-d", filectime($archivo));

        $archivos[] = $file;

		return $archivos;
	}


	function getDescargaReporteLotizacion(){
		$hora = date("h:i:s");
    	$fecha = date("Y-m-j");

		$sql="	SELECT U.ubicacion, A.des_area_ran, A.area_ini_ran, A.area_fin_ran, ((A.area_fin_ran - A.area_ini_ran)+1) cantidad, 
				(SELECT COUNT(J.lote) FROM justificacion J WHERE J.lote BETWEEN A.area_ini_ran AND A.area_fin_ran) justificados,
				(SELECT SUM(cant_cap) FROM captura C WHERE C.area_cap BETWEEN A.area_ini_ran AND A.area_fin_ran) contados,
				(SELECT SUM(C.cant_cap * M.Prec_barra) FROM captura C LEFT JOIN maestro M ON C.barra_cap = M.cod_barra WHERE C.area_cap BETWEEN A.area_ini_ran AND A.area_fin_ran) contados_soles
				FROM area_rango A LEFT JOIN ubicacion U
				ON A.ubicacion = U.idUbicacion";
		$res = $this->db->get_results($sql);
		$this->_codificarObjeto($res,array("des_area_ran"));

							
			date_default_timezone_set('America/Mexico_City');
			define('FORMAT_CURRENCY_SOLES', '_-* #,##0.00\ [$zł-415]_-');

			if (PHP_SAPI == 'cli')
				die('Este archivo solo se puede ver desde un navegador web');

			/** Se agrega la libreria PHPExcel */
			//require_once 'lib/PHPExcel/PHPExcel.php';
			$borde_celda = array( 
				'borders' => array(
				    'outline' => array(
				    	'style' => PHPExcel_Style_Border::BORDER_THIN
				    )
				)
			);

			// Se crea el objeto PHPExcel
			$objPHPExcel = new PHPExcel();

			$objReader = PHPExcel_IOFactory::createReader('Excel2007');
			$objPHPExcel = $objReader->load("formatos/reporte_lotizacion.xlsx");


			//DAR VUELTA A TODOS LOS DATOS PARA QUE SE CARGUE LA GRILLA

			$i = 6;
			for( $x = 0; $x < count($res); $x++) { 
				if($res[$x]->ubicacion == "BODEGA"){	
					$objPHPExcel->setActiveSheetIndex(0)
								->setCellValue('A'.$i,$res[$x]->des_area_ran)
								->setCellValue('B'.$i,$res[$x]->area_ini_ran)
								->setCellValue('C'.$i,$res[$x]->area_fin_ran)
								->setCellValue('D'.$i,$res[$x]->cantidad)
								->setCellValue('E'.$i,$res[$x]->justificados)
								->setCellValue('F'.$i,$res[$x]->contados)
								->setCellValue('G'.$i,$res[$x]->contados_soles);

					$i++;
				}

			}
			$objPHPExcel->getActiveSheet()->getStyle('A6:G'.($i-1))->applyFromArray($borde_celda);
			$objPHPExcel->getActiveSheet()->getStyle('F6:G'.($i-1))->getNumberFormat()->setFormatCode('#,##0.00');

			$i = 6;
			for( $x = 0; $x < count($res); $x++) { 
				if($res[$x]->ubicacion == "SALA"){	
					$objPHPExcel->setActiveSheetIndex(1)
								->setCellValue('A'.$i,$res[$x]->des_area_ran)
								->setCellValue('B'.$i,$res[$x]->area_ini_ran)
								->setCellValue('C'.$i,$res[$x]->area_fin_ran)
								->setCellValue('D'.$i,$res[$x]->cantidad)
								->setCellValue('E'.$i,$res[$x]->justificados)
								->setCellValue('F'.$i,$res[$x]->contados)
								->setCellValue('G'.$i,$res[$x]->contados_soles);
					$i++;
				}

			}
			$objPHPExcel->getActiveSheet()->getStyle('A6:G'.($i-1))->applyFromArray($borde_celda);
			$objPHPExcel->getActiveSheet()->getStyle('F6:G'.($i-1))->getNumberFormat()->setFormatCode('#,##0.00');

			$objPHPExcel->setActiveSheetIndex(0);




			// Se manda el archivo al navegador web, con el nombre que se indica (Excel2007)
			header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			header('Content-Disposition: attachment;filename="reporte_lotizacion.xlsx"');
			header('Cache-Control: max-age=0');

			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
			$objWriter->save('reportes/reporte_lotizacion.xlsx');

			return "ok";
			

	}

	function getDescargaReporteGrafico(){
		$hora = date("h:i:s");
    	$fecha = date("Y-m-j");

    	$sqlTrucate =  "TRUNCATE TABLE grafico_tot";
    	$resTrucate=$this->db->query($sqlTrucate);

    	$sqlTrucate =  "TRUNCATE TABLE reporte_area_rango";
    	$resTrucate=$this->db->query($sqlTrucate);

    	$sqlInsert = "	INSERT INTO grafico_tot

						SELECT 0 idGrafico, m.linea, c.cant_cap, m.precio prec_barra, g.njerarquias jerarquia, GROUP_CONCAT(DISTINCT ar.ubicacion SEPARATOR ',') ubicacion 
						FROM captura c LEFT JOIN area_rango ar
						ON c.area_cap BETWEEN ar.area_ini_ran AND ar.area_fin_ran LEFT JOIN maestro m
						ON c.barra_cap = m.cod_barra LEFT JOIN grupos g
						ON m.linea = g.idGrupo
						GROUP BY c.id_captura ";
		$resInsert=$this->db->query($sqlInsert);

    	$sqlInsert = "	INSERT INTO reporte_area_rango
						SELECT
						GROUP_CONCAT(DISTINCT ar.ubicacion SEPARATOR '-') ubicacion, c.sku_cap FROM captura c LEFT JOIN area_rango ar
						ON c.area_cap BETWEEN ar.area_ini_ran AND ar.area_fin_ran
						GROUP BY c.sku_cap ";
		$resInsert=$this->db->query($sqlInsert);


		$sql = "SELECT g.ubicacion, g.jerarquia, u.ubicacion, SUM(g.cant_cap) unidades, SUM(g.cant_cap * g.prec_barra) soles FROM grafico_tot g LEFT JOIN ubicacion u
				ON g.ubicacion = u.idUbicacion
				
				GROUP BY g.jerarquia, u.ubicacion
				ORDER BY u.ubicacion, g.jerarquia ASC"; //WHERE g.jerarquia IN ('Bijouterie')
		$res = $this->db->get_results($sql);
		$this->_codificarObjeto($res,array("jerarquia","ubicacion"));

		$sqlTotal="	SELECT g.jerarquia, SUM(g.cant_cap) unidades, SUM(g.cant_cap * g.prec_barra) soles FROM grafico_tot g LEFT JOIN ubicacion u
					ON g.ubicacion = u.idUbicacion
					
					GROUP BY g.jerarquia
					ORDER BY u.ubicacion, g.jerarquia ASC"; //WHERE g.jerarquia IN ('Bijouterie')
		$resTotal = $this->db->get_results($sqlTotal);
		$this->_codificarObjeto($resTotal,array("jerarquia"));

		$sqlPrincipal="	SELECT us.descripcion, COUNT(rar.sku) sku FROM reporte_area_rango rar LEFT JOIN ubicacion_separado us
					ON rar.ubicacion = us.ubicacion
					GROUP BY us.descripcion";
		$resPrincipal = $this->db->get_results($sqlPrincipal);
		$this->_codificarObjeto($resPrincipal,array("descripcion"));


			date_default_timezone_set('America/Mexico_City');

			if (PHP_SAPI == 'cli')
				die('Este archivo solo se puede ver desde un navegador web');

			// Se crea el objeto PHPExcel
			$objPHPExcel = new PHPExcel();

			$objReader = PHPExcel_IOFactory::createReader('Excel2007');
			$objReader->setIncludeCharts(TRUE);
			$objPHPExcel = $objReader->load("formatos/reporte_grafico_uni_sol.xlsx");


			//DAR VUELTA A TODOS LOS DATOS PARA QUE SE CARGUE LA GRILLA
			$suma_bodega = 0;
			$suma_sala = 0;
			$suma_bodega_sala = 0;
			$suma_sku_total = 0;
			
			$suma_uni_uno = 0;
			$suma_uni_dos = 0;
			$suma_uni_tres = 0;
			$suma_uni_cuatro = 0;
			$suma_uni_cinco = 0;
			$suma_uni_seis = 0;
			$suma_uni_siete = 0;
			$suma_uni_ocho = 0;
			$suma_uni_nueve = 0;
			$suma_total_uni = 0;

			$suma_sol_uno = 0;
			$suma_sol_dos = 0;
			$suma_sol_tres = 0;
			$suma_sol_cuatro = 0;
			$suma_sol_cinco = 0;
			$suma_sol_seis = 0;
			$suma_sol_siete = 0;
			$suma_sol_ocho = 0;
			$suma_sol_nueve = 0;
			$suma_total_sol = 0;

			$uni_b_uno = $uni_s_uno = $sol_b_uno = $sol_s_uno = 0;
			$uni_b_dos = $uni_s_dos = $sol_b_dos = $sol_s_dos = 0;
			$uni_b_tres = $uni_s_tres = $sol_b_tres = $sol_s_tres = 0;
			$uni_b_cuatro = $uni_s_cuatro = $sol_b_cuatro = $sol_s_cuatro = 0;
			$uni_b_cinco = $uni_s_cinco = $sol_b_cinco = $sol_s_cinco = 0;
			$uni_b_seis = $uni_s_seis = $sol_b_seis = $sol_s_seis = 0;
			$uni_b_siete = $uni_s_siete = $sol_b_siete = $sol_s_siete = 0;
			$uni_b_ocho = $uni_s_ocho = $sol_b_ocho = $sol_s_ocho = 0;
			$uni_b_nueve = $uni_s_nueve = $sol_b_nueve = $sol_s_nueve = 0;

			$uni_total_b = $uni_total_s = $sol_total_b = $sol_total_s = 0;

			//$i = 6;
			for ($y=0; $y < count($resPrincipal); $y++){
				if ($resPrincipal[$y]->descripcion == "BODEGA"){
					$suma_bodega = $suma_bodega + $resPrincipal[$y]->sku;
				}
				if ($resPrincipal[$y]->descripcion == "SALA"){
					$suma_sala = $suma_sala + $resPrincipal[$y]->sku;
				}
				if ($resPrincipal[$y]->descripcion == "BODEGA Y SALA"){
					$suma_bodega_sala = $suma_bodega_sala + $resPrincipal[$y]->sku;
				}
					$suma_sku_total = $suma_sku_total + $resPrincipal[$y]->sku;
			}

			for ($i=0; $i < count($resTotal); $i++){
				if ($resTotal[$i]->jerarquia == "Accesorios Pelo"){
					$suma_uni_uno = $suma_uni_uno + $resTotal[$i]->unidades;
					$suma_sol_uno = $suma_sol_uno + $resTotal[$i]->soles;
				}
				if ($resTotal[$i]->jerarquia == "Accesorios Textiles"){
					$suma_uni_dos = $suma_uni_dos + $resTotal[$i]->unidades;
					$suma_sol_dos = $suma_sol_dos + $resTotal[$i]->soles;
				}
				if ($resTotal[$i]->jerarquia == "Bijouterie"){
					$suma_uni_tres = $suma_uni_tres + $resTotal[$i]->unidades;
					$suma_sol_tres = $suma_sol_tres + $resTotal[$i]->soles;
				}
				if ($resTotal[$i]->jerarquia == "Complementos"){
					$suma_uni_cuatro = $suma_uni_cuatro + $resTotal[$i]->unidades;
					$suma_sol_cuatro = $suma_sol_cuatro + $resTotal[$i]->soles;
				}
				if ($resTotal[$i]->jerarquia == "Cosméticos"){
					$suma_uni_cinco = $suma_uni_cinco + $resTotal[$i]->unidades;
					$suma_sol_cinco = $suma_sol_cinco + $resTotal[$i]->soles;
				}
				if ($resTotal[$i]->jerarquia == "Indumentaria"){
					$suma_uni_seis = $suma_uni_seis + $resTotal[$i]->unidades;
					$suma_sol_seis = $suma_sol_seis + $resTotal[$i]->soles;
				}
				if ($resTotal[$i]->jerarquia == "Marroquinería Chica"){
					$suma_uni_siete = $suma_uni_siete + $resTotal[$i]->unidades;
					$suma_sol_siete = $suma_sol_siete + $resTotal[$i]->soles;
				}
				if ($resTotal[$i]->jerarquia == "Marroquinería Grande"){
					$suma_uni_ocho = $suma_uni_ocho + $resTotal[$i]->unidades;
					$suma_sol_ocho = $suma_sol_ocho + $resTotal[$i]->soles;
				}
				if ($resTotal[$i]->jerarquia == "Regalería"){
					$suma_uni_nueve = $suma_uni_nueve + $resTotal[$i]->unidades;
					$suma_sol_nueve = $suma_sol_nueve + $resTotal[$i]->soles;
				}
					$suma_total_uni = $suma_total_uni + $resTotal[$i]->unidades;
					$suma_total_sol = $suma_total_sol + $resTotal[$i]->soles;
			}


			for( $x = 0; $x < count($res); $x++) { 
				if($res[$x]->ubicacion == "BODEGA"){
					if($res[$x]->jerarquia == "Accesorios Pelo"){
						$uni_b_uno = $res[$x]->unidades / $suma_uni_uno;
						$sol_b_uno = $res[$x]->soles / $suma_sol_uno;
					}
					if($res[$x]->jerarquia == "Accesorios Textiles"){
						$uni_b_dos = $res[$x]->unidades / $suma_uni_dos;
						$sol_b_dos = $res[$x]->soles / $suma_sol_dos;
					}
					if($res[$x]->jerarquia == "Bijouterie"){
						$uni_b_tres = $res[$x]->unidades / $suma_uni_tres;
						$sol_b_tres = $res[$x]->soles / $suma_sol_tres;
					}
					if($res[$x]->jerarquia == "Complementos"){
						$uni_b_cuatro = $res[$x]->unidades / $suma_uni_cuatro;
						$sol_b_cuatro = $res[$x]->soles / $suma_sol_cuatro;
					}
					if($res[$x]->jerarquia == "Cosméticos"){
						$uni_b_cinco = $res[$x]->unidades / $suma_uni_cinco;
						$sol_b_cinco = $res[$x]->soles / $suma_sol_cinco;
					}
					if($res[$x]->jerarquia == "Indumentaria"){
						$uni_b_seis = $res[$x]->unidades / $suma_uni_seis;
						$sol_b_seis = $res[$x]->soles / $suma_sol_seis;
					}
					if($res[$x]->jerarquia == "Marroquinería Chica"){
						$uni_b_siete = $res[$x]->unidades / $suma_uni_siete;
						$sol_b_siete = $res[$x]->soles / $suma_sol_siete;
					}
					if($res[$x]->jerarquia == "Marroquinería Grande"){
						$uni_b_ocho = $res[$x]->unidades / $suma_uni_ocho;
						$sol_b_ocho = $res[$x]->soles / $suma_sol_ocho;
					}
					if($res[$x]->jerarquia == "Regalería"){
						$uni_b_nueve = $res[$x]->unidades / $suma_uni_nueve;
						$sol_b_nueve = $res[$x]->soles / $suma_sol_nueve;
					}
					$uni_total_b = $uni_total_b + $res[$x]->unidades;
					$sol_total_b = $sol_total_b + $res[$x]->soles;
				}
				if($res[$x]->ubicacion == "SALA"){
					if($res[$x]->jerarquia == "Accesorios Pelo"){
						$uni_s_uno = $res[$x]->unidades / $suma_uni_uno;
						$sol_s_uno = $res[$x]->soles / $suma_sol_uno;
					}
					if($res[$x]->jerarquia == "Accesorios Textiles"){
						$uni_s_dos = $res[$x]->unidades / $suma_uni_dos;
						$sol_s_dos = $res[$x]->soles / $suma_sol_dos;
					}
					if($res[$x]->jerarquia == "Bijouterie"){
						$uni_s_tres = $res[$x]->unidades / $suma_uni_tres;
						$sol_s_tres = $res[$x]->soles / $suma_sol_tres;
					}
					if($res[$x]->jerarquia == "Complementos"){
						$uni_s_cuatro = $res[$x]->unidades / $suma_uni_cuatro;
						$sol_s_cuatro = $res[$x]->soles / $suma_sol_cuatro;
					}
					if($res[$x]->jerarquia == "Cosméticos"){
						$uni_s_cinco = $res[$x]->unidades / $suma_uni_cinco;
						$sol_s_cinco = $res[$x]->soles / $suma_sol_cinco;
					}
					if($res[$x]->jerarquia == "Indumentaria"){
						$uni_s_seis = $res[$x]->unidades / $suma_uni_seis;
						$sol_s_seis = $res[$x]->soles / $suma_sol_seis;
					}
					if($res[$x]->jerarquia == "Marroquinería Chica"){
						$uni_s_siete = $res[$x]->unidades / $suma_uni_siete;
						$sol_s_siete = $res[$x]->soles / $suma_sol_siete;
					}
					if($res[$x]->jerarquia == "Marroquinería Grande"){
						$uni_s_ocho = $res[$x]->unidades / $suma_uni_ocho;
						$sol_s_ocho = $res[$x]->soles / $suma_sol_ocho;
					}
					if($res[$x]->jerarquia == "Regalería"){
						$uni_s_nueve = $res[$x]->unidades / $suma_uni_nueve;
						$sol_s_nueve = $res[$x]->soles / $suma_sol_nueve;
					}
					$uni_total_s = $uni_total_s + $res[$x]->unidades;
					$sol_total_s = $sol_total_s + $res[$x]->soles;
				}
			}


	
			$objPHPExcel->setActiveSheetIndex(0)
				->setCellValue('E4',$suma_bodega)
				->setCellValue('E5',$suma_sala)
				->setCellValue('E6',$suma_bodega_sala)
				->setCellValue('E7',$suma_sku_total)
				->setCellValue('H4',($suma_bodega / $suma_sku_total))
				->setCellValue('H5',($suma_sala / $suma_sku_total))
				->setCellValue('H6',($suma_bodega_sala / $suma_sku_total))
				->setCellValue('H7',($suma_sku_total / $suma_sku_total))
				->setCellValue('C12',$uni_b_uno)
				->setCellValue('D12',$uni_s_uno)
				->setCellValue('H12',$sol_b_uno)
				->setCellValue('I12',$sol_s_uno)
				->setCellValue('C13',$uni_b_dos)
				->setCellValue('D13',$uni_s_dos)
				->setCellValue('H13',$sol_b_dos)
				->setCellValue('I13',$sol_s_dos)
				->setCellValue('C14',$uni_b_tres)
				->setCellValue('D14',$uni_s_tres)
				->setCellValue('H14',$sol_b_tres)
				->setCellValue('I14',$sol_s_tres)
				->setCellValue('C15',$uni_b_cuatro)
				->setCellValue('D15',$uni_s_cuatro)
				->setCellValue('H15',$sol_b_cuatro)
				->setCellValue('I15',$sol_s_cuatro)
				->setCellValue('C16',$uni_b_cinco)
				->setCellValue('D16',$uni_s_cinco)
				->setCellValue('H16',$sol_b_cinco)
				->setCellValue('I16',$sol_s_cinco)
				->setCellValue('C17',$uni_b_seis)
				->setCellValue('D17',$uni_s_seis)
				->setCellValue('H17',$sol_b_seis)
				->setCellValue('I17',$sol_s_seis)
				->setCellValue('C18',$uni_b_siete)
				->setCellValue('D18',$uni_s_siete)
				->setCellValue('H18',$sol_b_siete)
				->setCellValue('I18',$sol_s_siete)
				->setCellValue('C19',$uni_b_ocho)
				->setCellValue('D19',$uni_s_ocho)
				->setCellValue('H19',$sol_b_ocho)
				->setCellValue('I19',$sol_s_ocho)
				->setCellValue('C20',$uni_b_nueve)
				->setCellValue('D20',$uni_s_nueve)
				->setCellValue('H20',$sol_b_nueve)
				->setCellValue('I20',$sol_s_nueve)
				//->setCellValue('C13',$uni_b_perecederos)
				//->setCellValue('C14',$uni_b_pgc)
				//->setCellValue('C15',$uni_b_ins)
				
				//->setCellValue('D13',$uni_s_perecederos)
				//->setCellValue('D14',$uni_s_pgc)
				//->setCellValue('D15',$uni_s_ins)
				
				//->setCellValue('H13',$sol_b_perecederos)
				//->setCellValue('H14',$sol_b_pgc)
				//->setCellValue('H15',$sol_b_ins)
				
				//->setCellValue('I13',$sol_s_perecederos)
				//->setCellValue('I14',$sol_s_pgc)
				//->setCellValue('I15',$sol_s_ins)
				->setCellValue('C21',($uni_total_b / $suma_total_uni))
				->setCellValue('D21',($uni_total_s / $suma_total_uni))
				->setCellValue('H21',($sol_total_b / $suma_total_sol))
				->setCellValue('I21',($sol_total_s / $suma_total_sol));



			//$objPHPExcel->setActiveSheetIndex(0);







			// Se manda el archivo al navegador web, con el nombre que se indica (Excel2007)
			header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			header('Content-Disposition: attachment;filename="reporte_grafico_uni_sol.xlsx"');
			header('Cache-Control: max-age=0');
			

			$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
			$objWriter->setIncludeCharts(TRUE);
			$objWriter->save('reportes/reporte_grafico_uni_sol.xlsx');

			return "ok";
			

	}

}	
?>