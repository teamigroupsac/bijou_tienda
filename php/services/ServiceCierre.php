<?php
ini_set('max_execution_time', 0);
require_once(INCDIR.'uc_functions.php');
require_once(INCDIR.'ez_sql/ez_sql_core.php');
require_once(INCDIR.'ez_sql/ez_sql_mysql.php');

require_once("Service.php");

class ServiceCierre extends Service
{
	
	function __construct() 
	{
		parent::__construct();
	}


	function generarCierre(){

		$sql_consulta = "SELECT COUNT(*) AS cuenta FROM captura WHERE sku_cap = ''";
		$res_consulta = $this->db->get_var($sql_consulta);

		if($res_consulta == 0){
			//LIMPIAR BASES TEMPORALES
			$sql="TRUNCATE capturas";
			$res=$this->db->query($sql);
			$sql="TRUNCATE diferencias";
			$res=$this->db->query($sql);

			$archivo = "../archivos_sistema/archivo_cierre.txt";
			unlink($archivo);

			//GENERAR TABLA CAPTURAS
			$sql="INSERT INTO capturas SELECT A.id_captura,A.area_cap,A.barra_cap,
					B.sku AS sku_cap,
					SUM(A.cant_cap) AS cant_cap,
					A.tip_cap,A.usuario,A.fecha,A.hora,
					B.des_barra,B.linea,B.des_linea,B.Precio
					FROM captura A LEFT JOIN maestro B
					ON A.barra_cap = B.cod_barra OR A.barra_cap = B.sku
					GROUP BY B.sku";
			$res=$this->db->query($sql);

			$sql="INSERT INTO diferencias SELECT s.sku AS sap, s.des_barra, s.cod_barra, s.linea, ROUND( s.precio, 2 ) AS costo, 
IFNULL( s.stock, 0 ) AS stock,  IFNULL( SUM(ct.cant_cap),0 )  AS contado, (IFNULL(s.stock,0) - IFNULL(SUM(ct.cant_cap),0)) AS dif_cant, 
IFNULL( s.stock * ROUND( s.precio, 2 ) , 0 ) AS total_stock_sol, IFNULL( IFNULL(SUM(ct.cant_cap),0 ) * ROUND( s.precio, 2 ) , 0 ) AS total_cap_sol, 
IFNULL( s.stock * ROUND( s.precio, 2 ),0) - IFNULL(IFNULL(SUM(ct.cant_cap),0) * ROUND( s.precio, 2 ) , 0 ) AS dif_sol, s.des_linea
FROM stock s
LEFT JOIN capturas ct ON s.sku = ct.sku_cap
GROUP BY s.sku
UNION DISTINCT SELECT ct.sku_cap AS sap, ct.des_barra, ct.barra_cap, ct.linea, ROUND( ct.precio, 2 ) AS costo, 
IFNULL( s.stock, 0 ) AS stock,  IFNULL( SUM(ct.cant_cap),0 )  AS contado, (IFNULL(s.stock,0) - IFNULL(SUM(ct.cant_cap),0)) AS dif_cant, 
IFNULL( s.stock * ROUND( s.precio, 2 ) , 0 ) AS total_stock_sol, IFNULL( IFNULL(SUM(ct.cant_cap),0 ) * ROUND( ct.precio, 2 ) , 0 ) AS total_cap_sol, 
IFNULL( s.stock * ROUND( s.precio, 2 ),0) - IFNULL(IFNULL(SUM(ct.cant_cap),0) * ROUND( ct.precio, 2 ) , 0 ) AS dif_sol, s.des_linea
FROM stock s
RIGHT JOIN capturas ct ON ct.sku_cap = s.sku where s.sku is null
GROUP BY ct.sku_cap";
			$res=$this->db->query($sql);


			$cadena ="ARCHIVO QUE INDICA LA FECHA DE CREACION";
			$cadenax = substr($cadena,1,strlen($cadena));
			$fch= fopen($archivo, "w"); // Abres el archivo para escribir en él
			fwrite($fch, $cadenax); // Grabas
			fclose($fch); // Cierras el archivo.

			return 1;			
		}else{
			return 0;
		}

	}

	function sincronizarInventario(){

	$cliente = $this->getDato("cliente","tienda","idTienda > 0 limit 1");
	$tienda = $this->getDato("numeroTienda","tienda","idTienda > 0 limit 1");


	$fecha = date("Y-m-d H:i:s");
	$fecha_archivo = date("Y_m_d H_i_s");
	$array_avance = array();
    $array_avance_total = array();
    $array_diferencia_total = array();
    $array_sub_diferencia_total = array();

    $valor_avance = '';
    $valor_avance_total = '';
    $valor_diferencia_total = '';
    $valor_sub_diferencia_total = '';

    $valor_ranking_productos = '';


    $service = new ServiceReportes();

    $resultado = $service->getListaReporteAreaRango();
    $dataRangos = $resultado->rangos;
    $dataCapturas = $resultado->capturas;
    $dataJustificados = $resultado->justificados;

    $cantidad_total = 0;
    $avance_total = 0;

    $cuentaRangos = count($dataRangos);

        for($i=0 ; $i < $cuentaRangos ; $i++){
        	$filaInicio = $dataRangos[$i]->area_ini_ran;
            $filaFinal = $dataRangos[$i]->area_fin_ran;
            $filaDescripcion = $dataRangos[$i]->des_area_ran;
            $cantidad = ( (int)$filaFinal - (int)$filaInicio ) + 1;
            $avance = 0;
            $porcentaje = 0;
            for ($z=$filaInicio; $z<=$filaFinal; $z++) { 
                for ($y=0; $y<count($dataCapturas); $y++) {
                    $valorArea = (int)$dataCapturas[$y]->area_cap;
                    if ($z == $valorArea){
                        $avance++;
                    }
                }
                for ($x=0; $x<count($dataJustificados); $x++) {
                    $valorArea = (int)$dataJustificados[$x]->lote;
                    if ($z == $valorArea){
                        $avance++;
                    }
                }
            }

            $cantidad_total = $cantidad_total + $cantidad;
            $avance_total = $avance_total + $avance;

            $porcentaje = round(($avance / $cantidad)*100,2);


            if($valor_avance_total != ""){
            	$valor_avance_total .= ",";
            }

            //$array_avance_total[] = array('area_rango' => $filaDescripcion, 'avance' => $porcentaje, 'cliente' => $cliente,'tienda' => $tienda,'fecha' => $fecha);

            $valor_avance_total .= "('$filaDescripcion',$porcentaje,'$cliente', '$tienda','$fecha')"; 

        }

        $valor_avance_total .= ";";

        $porcentaje_total = round(($avance_total / $cantidad_total)*100,2);

        //AVANCE
        $avance = $service->getAvanceTotal();
        for($i=0 ; $i < count($avance) ; $i++){
        	//$array_avance[] = array('porcentaje' => $porcentaje_total, 'stock' => $avance[$i]->stock, 'captura' => $avance[$i]->captura, 'stock_sol' => $avance[$i]->stock_sol, 'captura_sol' => $avance[$i]->captura_sol, 'cliente' => $cliente,'tienda' => $tienda,'fecha' => $fecha);
        	$valor_avance .= "($porcentaje_total,".$avance[$i]->stock.", ".$avance[$i]->captura.",".$avance[$i]->stock_sol.", ".$avance[$i]->captura_sol.",'$cliente', '$tienda','$fecha')";
        }
        //FIN AVANCE

        //DIFERENCIA
        $diferenciaTotal = $service->getDiferenciaTotal();
        for($i=0 ; $i < count($diferenciaTotal) ; $i++){
        	if($valor_diferencia_total != ""){
            	$valor_diferencia_total .= ",";
            }
        	//$array_diferencia_total[] = array('departamento' => $diferenciaTotal[$i]->departamento, 'diferencia_soles' => $diferenciaTotal[$i]->diferencia_soles, 'diferencia_cantidad' => $diferenciaTotal[$i]->diferencia_cantidad, 'cliente' => $cliente,'tienda' => $tienda,'fecha' => $fecha);
        	$valor_diferencia_total .= "('".$diferenciaTotal[$i]->departamento."','".$diferenciaTotal[$i]->departamento_original."',".$diferenciaTotal[$i]->diferencia_soles.", ".$diferenciaTotal[$i]->diferencia_cantidad.",'$cliente', '$tienda','$fecha')";

        	$rankingProductos = $service->getRankingProductos($diferenciaTotal[$i]->departamento_original);
	        for($x=0 ; $x < count($rankingProductos) ; $x++){
	        	if($valor_ranking_productos != ""){
	            	$valor_ranking_productos .= ",";
	            }
	        	$valor_ranking_productos .= "('".$rankingProductos[$x]->sku_stk."','".$rankingProductos[$x]->des_sku_stk."','".$rankingProductos[$x]->costo."','".$rankingProductos[$x]->stock."','".$rankingProductos[$x]->contado."',".$rankingProductos[$x]->dif_cant.", ".$rankingProductos[$x]->dif_sol.", ".$rankingProductos[$x]->dif_sol_pos.", '".$rankingProductos[$x]->des_dep_stk."', '".$rankingProductos[$x]->des_sub_dep_stk."', '".$rankingProductos[$x]->des_clas_stk."','$cliente', '$tienda','$fecha')";
	        }

        }
        $valor_diferencia_total .= ";";

       	$valor_ranking_productos .= ";";


        $subDiferenciaTotal = $service->getSubDiferenciaTotal();
        for($i=0 ; $i < count($subDiferenciaTotal) ; $i++){
        	if($valor_sub_diferencia_total != ""){
            	$valor_sub_diferencia_total .= ",";
            }	
        	//$array_sub_diferencia_total[] = array('departamento' => $subDiferenciaTotal[$i]->departamento,'sub_departamento' => $subDiferenciaTotal[$i]->sub_departamento, 'diferencia_soles' => $subDiferenciaTotal[$i]->diferencia_soles, 'diferencia_cantidad' => $subDiferenciaTotal[$i]->diferencia_cantidad, 'cliente' => $cliente,'tienda' => $tienda,'fecha' => $fecha);
        	$valor_sub_diferencia_total .= "('".$subDiferenciaTotal[$i]->departamento."','".$subDiferenciaTotal[$i]->sub_departamento."', ".$subDiferenciaTotal[$i]->diferencia_soles.", ".$subDiferenciaTotal[$i]->diferencia_cantidad.", '$cliente', '$tienda','$fecha')";
        }
        $valor_sub_diferencia_total .= ";";

        //FIN DIFERENCIA

        //GENERAR ARCHIVOS PARA CARGA
        $service = new ServiceGenerarArchivos();
    	$primerArchivo = $service->generarprimerarchivo();
    	$segundoArchivo = $service->generarsegundoarchivo();
    	$tercerArcihvo = $service->generarbalarchivo();
        //CERRAR GENERAR ARCHIVOS

    	//COMPRIMIR ARCHIVOS
    	//var_dump($tienda);
    	//var_dump($fecha_archivo);
		require('../php/includes/pclzip/pclzip.lib.php');
		$files_to_zip = array('IF_CARGA.txt','BOD_SV.txt','26-09-12-2018.BAL');
		$dir = '../archivos_sistema/archivos_generados/'; 
		$new_files = array();
		  foreach($files_to_zip as $value){
		   $new_files[] = $dir.$value;
		}
		$archive = new PclZip("archivos_generados_".$cliente."".$tienda."".$fecha_archivo.".zip");
		$files_archive = $archive->add($new_files, PCLZIP_OPT_REMOVE_PATH, $dir, PCLZIP_OPT_ADD_PATH, 'archivos_generados');
		if ($files_archive == 0) {
			$archivo_zip = false;
		} else {
			$archivo_zip = true;
		}
    	//FINAL COMPRESION

        //CARGAR LOS RESULTADOS AL SISTEMA WEB
        
        $dbWeb = new mysqli("mysql.igroupsac.com", "user_igroupsac", "User210382!?", "database_igroupsac");

        $sqlAvance ="INSERT INTO avance (porcentaje, stock, captura, stock_sol, captura_sol, cliente, tienda, fecha) VALUES $valor_avance";
		$resAvance = $dbWeb->query($sqlAvance);

		$sqlAvanceTotal ="INSERT INTO avance_total (area_rango, avance, cliente, tienda, fecha) VALUES $valor_avance_total";
		$resAvanceTotal = $dbWeb->query($sqlAvanceTotal);

		$sqlDiferenciaTotal ="INSERT INTO diferencia_total (departamento, departamento_original, diferencia_soles, diferencia_cantidad, cliente, tienda, fecha) VALUES $valor_diferencia_total";
		$resDiferenciaTotal = $dbWeb->query($sqlDiferenciaTotal);

		$sqlSubDiferenciaTotal ="INSERT INTO sub_diferencia_total (departamento, sub_departamento, diferencia_soles, diferencia_cantidad, cliente, tienda, fecha) VALUES $valor_sub_diferencia_total";
		$resSubDiferenciaTotal = $dbWeb->query($sqlSubDiferenciaTotal);

		$sqlRankingProductos ="INSERT INTO ranking_productos (sku_stk, des_sku_stk, costo, stock, contado, dif_cant, dif_sol, dif_sol_pos, departamento, sub_departamento, producto, cliente, tienda, fecha) VALUES $valor_ranking_productos";
		$resRankingProductos = $dbWeb->query($sqlRankingProductos);

		//SUBIR ARCHIVO AL SERVIDOR
		$servidor_ftp = "cloverleaf.dreamhost.com";
		$conexion_id = ftp_connect($servidor_ftp);
		$ftp_usuario = "igroupsac";
		$ftp_clave = "Igroup210382!?";
		$ftp_carpeta_local =  "";
		$ftp_carpeta_remota = "/igroupsac.com/sistema/archivos_sistema/";
		$mi_nombredearchivo = "archivos_generados_".$cliente."".$tienda."".$fecha_archivo.".zip";
		$nombre_archivo = $ftp_carpeta_local.$mi_nombredearchivo;
		$archivo_destino = $ftp_carpeta_remota.$mi_nombredearchivo;
		$resultado_login = ftp_login($conexion_id, $ftp_usuario, $ftp_clave);
		if ((!$conexion_id) || (!$resultado_login)) {
		       $archivo_zip_cargado = false;
		   } else {
		       $archivo_zip_cargado = true;
		   }
		$upload = ftp_put($conexion_id, $archivo_destino, $nombre_archivo, FTP_BINARY);
		if (!$upload) {
		       $archivo_zip_cargado = false;
		   } else {
		       $archivo_zip_cargado = true;
		   }
		ftp_close($conexion_id);
		//FINAL SUBIR ARCHIVO


		//$resultado = new stdClass();
		//$resultado->avance = $valor_avance;
        //$resultado->avance_total = $valor_avance_total; //$array_avance_total;
        //$resultado->diferencia_total = $valor_diferencia_total;
        //$resultado->sub_diferencia_total = $valor_sub_diferencia_total;
        //var_dump($resAvance);
        //var_dump($resAvanceTotal);
        //var_dump($resDiferenciaTotal);
        //var_dump($resSubDiferenciaTotal);
        //var_dump($archivo_zip);
        //var_dump($archivo_zip_cargado);
        //var_dump($resRankingProductos);

        if($resAvance && $resAvanceTotal && $resDiferenciaTotal /*&& $resSubDiferenciaTotal*/ && $archivo_zip && $archivo_zip_cargado /*&& $resRankingProductos*/){
        	return 1;
        }else{
	        $sqlAvance ="DELETE FROM avance WHERE cliente = $cliente AND tienda = $tienda AND fecha = '$fecha'";
			$dbWeb->query($sqlAvance);

			$sqlAvanceTotal ="DELETE FROM avance_total WHERE cliente = $cliente AND tienda = $tienda AND fecha = '$fecha'";
			$dbWeb->query($sqlAvanceTotal);

			$sqlDiferenciaTotal ="DELETE FROM diferencia_total WHERE cliente = $cliente AND tienda = $tienda AND fecha = '$fecha'";
			$dbWeb->query($sqlDiferenciaTotal);

			$sqlSubDiferenciaTotal ="DELETE FROM sub_diferencia_total WHERE cliente = $cliente AND tienda = $tienda AND fecha = '$fecha'";
			$dbWeb->query($sqlSubDiferenciaTotal);

        	return 2;
        }

	}



	function comprobarInformacionCierre(){
		$archivo = "../archivos_sistema/archivo_cierre.txt";
		return "FECHA : ".date("Y-m-d", filemtime($archivo))." - HORA : ".date("H:i:s", filemtime($archivo));
	}


	function consultarRegistrosStock(){
		$sql = "SELECT COUNT(stock_id) cantidad FROM stock";
		$res = $this->db->get_var($sql);

		return $res;	
	}




}	
?>