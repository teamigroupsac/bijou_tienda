<div class="modal fade" tabindex="-1" role="dialog" id="modal_generar_cierre_progress" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog  modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="list-group">
                    <br><br>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="progress">
                                <div class="progress-bar progress-bar-success progress-bar-striped active descripcionarchivogenerado" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width:100%">GENERANDO CIERRE</div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" tabindex="-1" role="dialog" id="modal_grafico_reporte_jerarquico">
    <div class="modal-dialog  modal-grande" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">GRAFICO REPORTE JERARQUICO</h4>
            </div>
            <div class="modal-body" id="areaImpresionUsuarios">
                <div class="list-group">
                    
                    <button type="button" class="btn btn-default btn-sm imprimirgrafico">IMPRIMIR GRAFICO</button>
                    <div id="greportejerarquico" style="width: 1050px; height: 1600px">
                    </div>

                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" tabindex="-1" role="dialog" id="modal_reporte_grupos">
    <div class="modal-dialog  modal-lg" role="document">
        <div class="modal-content">
            <!--<div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">JUSTIFICAR AREA SIN CONTEO</h4>
            </div>-->
            <div class="modal-body">
                <div class="list-group">
                    
                    <div class="row">
                        <div class="col-md-2"><img src="img/logoReporte.png" height="39" width="87"></div>
                        <div class="col-md-8 texto negrita centro"><h2 class="modal-title">FORMULARIO REPORTE DE GRUPOS</h2></div>
                        <div class="col-md-2"></div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-12">
                            
                            <table width="100%" class="table table-striped table-bordered table-hover tablagrupos">
                                <thead>
                                    <tr>
                                        <th width="5%">#</th>
                                        <th width="10%">CODIGO</th>
                                        <th width="25%">JERARQUIAS</th>
                                        <th width="60%">DIVISION</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>

                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <button type="button" class="btn btn-success btn-sm btn-block guardarformulario">GUARDAR FORMULARIO</button>
                        </div>
                        <div class="col-md-6">
                            <button type="button" class="btn btn-info btn-sm btn-block imprimirreporte">IMPRIMIR REPORTE</button>
                        </div>
                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" tabindex="-1" role="dialog" id="modal_generar_archivos_progress" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog  modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <div class="list-group">
                    <br><br>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="progress">
                                <div class="progress-bar progress-bar-success progress-bar-striped active descripcionarchivogenerado" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width:100%">GENERANDO ARCHIVO</div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->