LOTE|SKU|DESCRIPCION|EAN|COSTO|STOCK|JERARQUIA|BODEGA|SALA
00269|0303464|REGLA 20CM TOP LINE|7750082207417|0.6290|5.000|0000J1007|1.000|0.000
02868|0303475|TRANSPORTADOR 180X10     |7750082207523|0.3170|10.000|0000J1007|0.000|1.000
00233|0303495|FOLDER OF. D/T C/G|7750082214613|2.8100|23.000|0000J1007|25.000|0.000
02868|0303495|FOLDER OF. D/T C/G|7750082214613|2.8100|23.000|0000J1007|0.000|1.000
02868|0303497|FOLDER A4 T/T C/G     |7750082400047|2.7090|4.000|0000J1007|0.000|7.000
04211|0303497|FOLDER A4 T/T C/G     |7750082400047|2.7090|4.000|0000J1007|0.000|5.000
00233|0303501|COMPAS PLASTICO CON LAPIZ|7750082745711|1.0460|4.000|0000J1007|1.000|0.000






SELECT c.area_cap, c.barra_cap, c.sku_cap, c.precio_barra, c.jerar, IF(sum(c.cant_cap)<>0,sum(c.cant_cap),0.000) as BODEGA, 
'0.000' as PISO_VENTA 
FROM captura_total c, rangos r where c.area_cap between r.inicio and r.fin and Id=1 
group by c.sku_cap, c.area_cap, Id 
UNION DISTINCT
SELECT c.area_cap, c.barra_cap, c.sku_cap, c.precio_barra, c.jerar, '0.000' as BODEGA, 
IF(sum(c.cant_cap)<>0,sum(c.cant_cap),0.000) as PISO_VENTA 
FROM captura_total c,rangos r where c.area_cap between r.inicio and r.fin and Id=2 
group by c.sku_cap, c.area_cap, Id order by sku_cap, area_cap