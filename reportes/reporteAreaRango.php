<?php 
    include("plantilla_reporte.php");
    error_reporting(0);
    session_start();
    require_once('../php/config.php');
    require_once('../php/services/ServiceReportes.php');
   
    //LLENADO DE DATOS

    $loteJusti = array();

    $service = new ServiceReportes();

    $resultado = $service->getListaReporteAreaRango();
    $dataRangos = $resultado->rangos;
    $dataCapturas = $resultado->capturas;
    $dataJustificados = $resultado->justificados;

    $cuentaRangos = count($dataRangos);


    $tamanoLetra = 8;

    $reportName = "REPORTE DE AREA RANGO";


    $borde = 0;
    $alineacion = "L";
    $altoFila = 4;

    $pdf = new PDF( 'P', 'mm', 'A4' );

    //foreach ($areas as $area) {


        $pdf->AddPage();

        $pdf->SetFont( 'Arial', 'B', $tamanoLetra );
        $pdf->Cell( 190, 5, $reportName, 0, 0, 'C' );
        $pdf->Ln(10);


        $pdf->SetFont( 'Arial', 'B', $tamanoLetra );
        $pdf->Cell( 10, $altoFila, 'N', $borde, 0, $alineacion);
        $pdf->Cell( 20, $altoFila, 'INICIO', $borde, 0, $alineacion);
        $pdf->Cell( 20, $altoFila, 'FINAL', $borde, 0, $alineacion);
        $pdf->Cell( 60, $altoFila, 'DESCRIPCION', $borde, 0, $alineacion);
        $pdf->Cell( 20, $altoFila, 'USUARIO', $borde, 0, $alineacion);
        $pdf->Cell( 20, $altoFila, 'CANTIDAD', $borde, 0, $alineacion);
        $pdf->Cell( 20, $altoFila, 'AVANCE', $borde, 0, $alineacion);
        $pdf->Cell( 20, $altoFila, '%', $borde, 0, $alineacion);
        $pdf->Ln($altoFila);





        for($i=0 ; $i < $cuentaRangos ; $i++){

            $filaInicio = $dataRangos[$i]->area_ini_ran;
            $filaFinal = $dataRangos[$i]->area_fin_ran;
            $filaDescripcion = $dataRangos[$i]->des_area_ran;
            $filaUsuario = $dataRangos[$i]->idUsuario;
            $cantidad = ( (int)$filaFinal - (int)$filaInicio ) + 1;
            $avance = 0;
            $porcentaje = 0;

            for ($z=$filaInicio; $z<=$filaFinal; $z++) { 
                for ($y=0; $y<count($dataCapturas); $y++) {
                    $valorArea = (int)$dataCapturas[$y]->area_cap;
                    if ($z == $valorArea){
                        $avance++;
                    }
                }

                for ($x=0; $x<count($dataJustificados); $x++) {
                    $valorArea = (int)$dataJustificados[$x]->lote;
                    if ($z == $valorArea){
                        $avance++;
                        $loteJusti[]= array('lote' => $dataJustificados[$x]->lote, 'justificacion' => $dataJustificados[$x]->justificacion);
                    }
                }


            }

            $porcentaje = round(($avance / $cantidad)*100,2);



            $pdf->SetFont( 'Arial', '', $tamanoLetra );
            $pdf->Cell( 10, $altoFila, $i+1, $borde, 0, $alineacion);
            $pdf->Cell( 20, $altoFila, $filaInicio, $borde, 0, $alineacion);
            $pdf->Cell( 20, $altoFila, $filaFinal, $borde, 0, $alineacion);
            $pdf->Cell( 60, $altoFila, $filaDescripcion, $borde, 0, $alineacion);
            $pdf->Cell( 20, $altoFila, $filaUsuario, $borde, 0, $alineacion);
            $pdf->Cell( 20, $altoFila, $cantidad, $borde, 0, $alineacion);
            $pdf->Cell( 20, $altoFila, $avance, $borde, 0, $alineacion);
            $pdf->Cell( 20, $altoFila, $porcentaje, $borde, 0, $alineacion);
            $pdf->Ln($altoFila);

            for ($k=0; $k<count($loteJusti); $k++) {
                $valor = $loteJusti[$k]["lote"].' : '.$loteJusti[$k]["justificacion"];
                $pdf->SetFont( 'Arial', '', $tamanoLetra );
                $pdf->Cell( 10, $altoFila, '', $borde, 0, $alineacion);
                $pdf->Cell( 20, $altoFila, '', $borde, 0, $alineacion);
                $pdf->Cell( 20, $altoFila, '', $borde, 0, $alineacion);
                $pdf->Cell( 60, $altoFila, $valor , $borde, 0, $alineacion);
                $pdf->Cell( 20, $altoFila, '', $borde, 0, $alineacion);
                $pdf->Cell( 20, $altoFila, '', $borde, 0, $alineacion);
                $pdf->Cell( 20, $altoFila, '', $borde, 0, $alineacion);
                $pdf->Cell( 20, $altoFila, '', $borde, 0, $alineacion);
                $pdf->Ln($altoFila);  
            }

            $loteJusti = [];

        }




    $pdf->Output( "reporte_area_rango.pdf", "I" );



?>
