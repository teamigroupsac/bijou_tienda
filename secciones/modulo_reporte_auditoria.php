                <!-- PAGINA PRINCIPAL DE INICIO -->
                <div class="tab-pane" id="reporte_auditoria">

                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            FORMULARIO REPORTE DE AUDITORIA
                        </div>
                        <div class="panel-body">
                            <br>
                            <div class="row">
                                <div class="col-md-1 texto negrita derecha">RESPONSABLE</div>
                                <div class="col-md-2">
                                    <select class="form-control input-sm responsable">
                                        <option value=''>TODOS</option>
                                        <option value='CLIENTE'>CLIENTE</option>
                                        <option value='IGROUP'>IGROUP</option>
                                    </select>
                                </div>
                                <div class="col-md-1 texto negrita derecha">TIPO</div>
                                <div class="col-md-1">
                                    <select class="form-control input-sm tipo">
                                        <option value=''>TODOS</option>
                                        <option value='EDITADO'>EDITADO</option>
                                        <option value='ELIMINADO'>ELIMINADO</option>
                                    </select>
                                </div>
                                <div class="col-md-2">
                                    <button type="button" class="btn btn-primary btn-block btn-sm dercargarreportedeauditoria">REPORTE DE AUDITORIA</button>
                                </div>
                            </div>
                            <br>
                        </div>

                    </div>
                </div>