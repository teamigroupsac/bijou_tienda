                <!-- PAGINA PRINCIPAL DE INICIO -->
                <div class="tab-pane" id="tipo_usuario">

                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            FORMULARIO TIPO DE USUARIO
                        </div>
                        <div class="panel-body">

                           

                                                    <br>
                                                        <div class="row contenedor opcionestipousuario">
                                                            <div class="col-md-1 texto negrita derecha">TIPO DE USUARIO</div>
                                                            <div class="col-md-3">
                                                                <input type="text" id="tipoUsuarioFormularioTipoUsuario" placeholder="TIPO DE USUARIO" class="form-control input-sm"></input>
                                                                <input type="hidden" id="codigoFormularioTipoUsuario"></input>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <!--<button id="cargarPermisosFormularioTipoUsuario" type="button" class="btn btn-primary btn-sm btn-block" data-toggle="modal">PERMISOS POR TIPO DE USUARIO</button>-->                                                           </div>
                                                            <div class="col-md-1"></div>
                                                            <div class="col-md-2"></div>
                                                            <div class="col-md-1"></div>
                                                            <div class="col-md-1"><button id="botonGuardarTipoUsuario" type="button" class="btn btn-success btn-sm btn-block">GUARDAR</button></div>
                                                            <div class="col-md-1"><button id="botonListarImprimirTipoUsuario" type="button" class="btn btn-default btn-sm btn-block"><span class="glyphicon glyphicon-print" aria-hidden="true"></span> IMPRIMIR</button></div>

                                                        </div>
                                                        <div class="row contenedor opcionestipousuario">
                                                            <div class="col-md-9"></div>
                                                            <div class="col-md-1"></div>
                                                            <div class="col-md-1"><button id="botonCancelarTipoUsuario" type="button" class="btn btn-danger btn-sm btn-block">CANCELAR</button></div>
                                                            <div class="col-md-1"></div>
                                                        </div>

                                                        <div class="row contenedor opcionestipousuariomasivo" style="display: none;">
                                                            <div class="col-md-2"></div>
                                                            <div class="col-md-1"></div>
                                                            <div class="col-md-2"></div>
                                                            <div class="col-md-2"></div>
                                                            <div class="col-md-1"></div>
                                                            <div class="col-md-2"></div>
                                                            <div class="col-md-2"></div>
                                                        </div>

                                                        <div class="row contenedor opcionestipousuariomasivo" style="display: none;">
                                                            <div class="col-md-5"></div>
                                                            <div class="col-md-2"><button id="botonEliminarMasivoFormularioTipoUsuario" type="button" class="btn btn-danger btn-sm btn-block">ELIMINAR SELECCIONADOS</button></div>
                                                            <div class="col-md-5"></div>
                                                        </div>

                                                        <br>
                                                        <table id="tablaResultadoFormularioTipoUsuario" width="100%" class="table table-striped table-bordered table-hover">
                                                            <thead>
                                                                <tr>
                                                                    <th width="5%">#</th>
                                                                    <th width="30%">TIPO USUARIO</th>
                                                                    <th width="40%">PERMISOS</th>
                                                                    <th width="10%">MARCAR</th>
                                                                    <th width="15%">OPCIONES</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                <tr>
                                                                </tr>                                              
                                                            </tbody>
                                                        </table>

                                                    <br>
                                                    <br>

                                                              



                    </div>
                </div>
            </div>

