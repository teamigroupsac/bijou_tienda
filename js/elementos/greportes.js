var service = new Service("webService/index.php");


$(function() {

iniciarAppResultados();

})

function iniciarAppResultados(){

//var lista_categorias = $("#categorias");
//var lista_colaboradores = $("#colaboradores");
//var lista_tipo_estudio = $("#tipodeestudio");

service.procesar("getReportes",crearGraficoReportes);

//lista_categorias.change(function(){

//service.procesar("getReportes",lista_categorias.val(),lista_colaboradores.val(),crearGraficoReportes);

//});

//lista_colaboradores.change(function(){

//service.procesar("getReportes",lista_categorias.val(),lista_colaboradores.val(),crearGraficoReportes);

//});


        function crearGraficoReportes(evt) {
            datos = evt;
            total = datos.total[0].RESULTADO;
            ensayo1 = datos.ensayo1[0].RESULTADO;
            ensayo2 = datos.ensayo2[0].RESULTADO;
            encuesta = datos.encuesta[0].RESULTADO;

            $("#chart").kendoChart({
                 title: {
                    text: "AVANCE DE ENCUESTAS"
                },
                legend: {
                    position: "bottom"
                },
                seriesDefaults: {
                    type: "column"
                },
                series: [{
                    name: "TOTAL_ENCUESTAS",
                    data: [total]
                }, {
                    name: "ENSAYO_01",
                    data: [ensayo1]
                }, {
                    name: "ENSAYO_02",
                    data: [ensayo2]
                }, {
                    name: "ENCUESTA_WEB",
                    data: [encuesta]
                }],
                valueAxis: {
                    line: {
                        visible: false
                    }
                },
                categoryAxis: {
                    categories: ["TOTAL DE ENCUESTAS RESUELTAS"],
                    majorGridLines: {
                        visible: false
                    }
                },
                tooltip: {
                    visible: true,
                    format: "{0}"
                }
            });
        }
        $(document).ready(crearGraficoReportes);
        $(document).bind("kendo:skinChange", crearGraficoReportes);
}