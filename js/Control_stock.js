var service = new Service("webService/index.php");


$(function() 
{

iniciarControlStock();

});

var arrayCapturas = [];

function iniciarControlStock(){
    var f = new Date();
    var mes = f.getMonth() < 9 ? "0" + (f.getMonth()+1) : f.getMonth()+1;
    var dia = f.getDate() < 10 ? "0" + (f.getDate()) : f.getDate();
    var fecha = f.getFullYear() +"-"+ mes +"-"+ dia;

    var usuario = sessionStorage.getItem("dniUsuario");

    var archivo = $("#archivoStock");
    //$('#fechaFormularioUsuario').val(fecha);

    




    $("#menu_stock").on('click', function(){
      cargaConsultasIniciales();  
    })
    $("#boton_menu_stock").on('click', function(){
      cargaConsultasIniciales();  
    })


    function cargaConsultasIniciales(){
        service.procesar("listarArchivosStockPendientes",cargaListarArchivosStockPendientes);
        //service.procesar("getListaAreaCap",cargaListaAreaCap);

        //$(".opcionesCaptura").css("display","none");
        //$(".opcionesMasivoCaptura").css("display","none");
        //service.procesar("getListaTipos",cargaListaTipos);
        //cargarPeriodos();
    }

    
    $("#fileCargaStock").on("click",function(){
        $("#inputCargaStock").trigger("click")
        alertify.success("PESO MAXIMO 2MB");
    })



    $(".boton-carga-stock").on('click',function(){
        archivo.trigger("click");

        archivo.change(function(){
            $(".campo-carga-stock").val( (archivo.val()).split('\\').pop() );
            $(".submit-carga-stock").prop("disabled",false);
        });
        

    });

    $(".boton-limpia-stock").on('click',function(){
        alertify.confirm("¿ SEGURO DE LIMPIAR LA INFORMACION ?", function (e) {
            if (e) {
                limpiarCargaStock();
            }
        });
    })

    $(".submit-carga-stock").on('click',function(){
        alertify.confirm("¿ SEGURO DE GUARDAR INFORMACION DEL ARCHIVO ?", function (e) {
            if (e) {
                //console.log(archivo[0].files[0].name);
                var valorArchivo = archivo[0].files[0];
                subirArchivo(valorArchivo,0,0);            
            }
        });

    })

    function limpiarCargaStock(){
        archivo.val(null);
        $(".campo-carga-stock").val("");
        $(".submit-carga-stock").prop("disabled",true);
    }







    function subirArchivo(archivo,datos,funcion){
        var data = new FormData();
        data.append("file",archivo);
        $.ajax({
            type: "POST",
            contentType: false,
            url: "webService/guardarArchivoStock.php",
            data: data,
            cache: false,
            processData: false,
            dataType:"json",
            success: function(evt){
                datos.link = evt.nombre;
                //funcion();
                //console.log("ARCHIVO GUARDADO...",evt.nombre);
                //service.procesar("listarArchivosMaestroPendientes",cargaListarArchivosMaestroPendientes);
                alertify.success("ARCHIVO CARGADO AL SERVIDOR");
                $(".progresoCargandoStock").css("display","");

                var objDatos = new Object()
                    objDatos.archivo = evt.nombre;
                service.procesar("saveRegistrosStock",objDatos,mensajeCargaStock);

            },
            error: function(evt){
                alertify.error("ARCHIVO NO CARGADO");
            }
        });
    }

    function mensajeCargaStock(evt){
        $(".progresoCargandoStock").css("display","none");
        limpiarCargaStock();
        service.procesar("listarArchivosStockPendientes",cargaListarArchivosStockPendientes);
        alertify.success("INFORMACION PROCESADA SATISFACTORIAMENTE");
    }

    function cargaListarArchivosStockPendientes(evt){
        resultado = evt;

        $("#tablaListaArchivosStockPendientes tbody").html("");

        if ( resultado == undefined ) return;

        for(var i=0; i<resultado.length ; i++){
            datoRegistro = resultado[i];

            var fila = $("<tr>");
            var celdaBotones = $("<td>");

            //(i + 1)
            fila.html('<td>'+(i+1)+'</td><td>'+ resultado[i].nombre +'</td><td>'+ resultado[i].fecha +'</td><td>'+ resultado[i].peso +'</td>'); //<td>'+ resultado[i].descripcion +'</td>

            var contenedorBotones = $("<td>");

            var btnInfo = $("<button class='btn btn-info btn-sm' title='VER LOG'>");
            var btnRemover = $("<button class='btn btn-danger btn-sm' title='EIMINAR'>");

            btnInfo.html('<span class="glyphicon glyphicon-eye-open"></span> ARCHIVO LOG');
            btnRemover.html('<span class="glyphicon glyphicon-remove"></span> ELIMINAR ARCHIVO');

            btnInfo.data("data",datoRegistro);
            btnRemover.data("data",datoRegistro);

            btnInfo.on("click",infoArchivosStockPendientes);
            btnRemover.on("click",removerArchivosStockPendientes);

            contenedorBotones.append(btnInfo);
            contenedorBotones.append(" ");
            contenedorBotones.append(btnRemover);

            fila.append(contenedorBotones);

            $("#tablaListaArchivosStockPendientes tbody").append(fila);

        }

    }

    function infoArchivosStockPendientes(){
        var data = $(this).data("data");
        window.open("archivos_sistema/archivos_stock/"+data.log, '_blank');
    }

    function removerArchivosStockPendientes(){
        var data = $(this).data("data");
        alertify.confirm("¿ SEGURO DE ELIMINAR EL ARCHIVO : " + data.nombre +" ?", function (e) {
            if (e) {
                service.procesar("eliminarArchivoStockPendiente",data.nombre,mensajeEliminarArchivoStockPendiente);
            }
        });
    }

    function mensajeEliminarArchivoStockPendiente(evt){
        if(evt > 0){
            service.procesar("listarArchivosStockPendientes",cargaListarArchivosStockPendientes);
            alertify.success("REGISTRO ELIMINADO CORRECTAMENTE");
        }else{
            alertify.error("REGISTRO NO ELIMINADO");
        }

    }
















    function getLista (array,atributo){
        var nuevoArray = []
        for ( i = 0; i < array.length; i++) {
            valor = array[i][atributo];
            nuevoArray.push(valor);
        }
        return nuevoArray;
    }

    function removeItemFromArr ( arr, item ) {
        var i = arr.indexOf( item );
     
        if ( i !== -1 ) {
            arr.splice( i, 1 );
        }
    }

}


