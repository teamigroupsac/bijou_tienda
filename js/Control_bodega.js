var service = new Service("webService/index.php");


$(function() 
{

iniciarControlBodega();

});

var arrayCapturas = [];

function iniciarControlBodega(){
    var f = new Date();
    var mes = f.getMonth() < 9 ? "0" + (f.getMonth()+1) : f.getMonth()+1;
    var dia = f.getDate() < 10 ? "0" + (f.getDate()) : f.getDate();
    var fecha = f.getFullYear() +"-"+ mes +"-"+ dia;

    var usuario = sessionStorage.getItem("dniUsuario");

    var archivoBodega = $("#archivoBodega");
    //$('#fechaFormularioUsuario').val(fecha);

    




    $("#menu_bodega").on('click', function(){
      cargaConsultasIniciales();  
    })
    $("#boton_menu_bodega").on('click', function(){
      cargaConsultasIniciales();  
    })

    function cargaConsultasIniciales(){
        service.procesar("listarArchivosBodegaPendientes",cargaListarArchivosBodegaPendientes);
        //service.procesar("getListaAreaCap",cargaListaAreaCap);

        //$(".opcionesCaptura").css("display","none");
        //$(".opcionesMasivoCaptura").css("display","none");
        //service.procesar("getListaTipos",cargaListaTipos);
        //cargarPeriodos();
    }

    /*
    $("#fileCargaBodega").on("click",function(){
        $("#inputCargaBodega").trigger("click")
        alertify.success("PESO MAXIMO 2MB");
    })
    */


    $(".boton-carga-bodega").on('click',function(){
        archivoBodega.trigger("click");
        archivoBodega.change(function(){
            $(".campo-carga-bodega").val( (archivoBodega.val()).split('\\').pop() );
            $(".submit-carga-bodega").prop("disabled",false);
        });
        

    });

    $(".boton-limpia-bodega").on('click',function(){
        alertify.confirm("¿ SEGURO DE LIMPIAR LA INFORMACION ?", function (e) {
            if (e) {
                limpiarCargaBodega();
            }
        });
    })

    $(".submit-carga-bodega").on('click',function(){
        alertify.confirm("¿ SEGURO DE GUARDAR INFORMACION DEL ARCHIVO ?", function (e) {
            if (e) {
                //console.log(archivo[0].files[0].name);
                var valorArchivo = archivoBodega[0].files[0];
                subirArchivo(valorArchivo,0,0);            
            }
        });

    })

    function limpiarCargaBodega(){
        archivoBodega.val(null);
        $(".campo-carga-bodega").val("");
        $(".submit-carga-bodega").prop("disabled",true);
    }







    function subirArchivo(archivoBodega,datos,funcion){
        var data = new FormData();
        data.append("file",archivoBodega);
        $.ajax({
            type: "POST",
            contentType: false,
            url: "webService/guardarArchivoBodega.php",
            data: data,
            cache: false,
            processData: false,
            dataType:"json",
            success: function(evt){
                datos.link = evt.nombre;
                //funcion();
                //console.log("ARCHIVO GUARDADO...",evt.nombre);
                //service.procesar("listarArchivosMaestroPendientes",cargaListarArchivosMaestroPendientes);
                alertify.success("ARCHIVO CARGADO AL SERVIDOR");
                $(".progresoCargandoBodega").css("display","");

                var objDatos = new Object()
                    objDatos.archivo = evt.nombre;
                service.procesar("saveRegistrosBodega",objDatos,mensajeCargaBodega);

            },
            error: function(evt){
                alertify.error("ARCHIVO NO CARGADO");
            }
        });
    }

    function mensajeCargaBodega(evt){
        $(".progresoCargandoBodega").css("display","none");
        limpiarCargaBodega();
        service.procesar("listarArchivosBodegaPendientes",cargaListarArchivosBodegaPendientes);
        alertify.success("INFORMACION PROCESADA SATISFACTORIAMENTE");
    }

    function cargaListarArchivosBodegaPendientes(evt){
        resultado = evt;

        $("#tablaListaArchivosBodegaPendientes tbody").html("");

        if ( resultado == undefined ) return;

        for(var i=0; i<resultado.length ; i++){
            datoRegistro = resultado[i];

            var fila = $("<tr>");
            var celdaBotones = $("<td>");

            //(i + 1)
            fila.html('<td>'+(i+1)+'</td><td>'+ resultado[i].nombre +'</td><td>'+ resultado[i].fecha +'</td><td>'+ resultado[i].peso +'</td>'); //<td>'+ resultado[i].descripcion +'</td>

            var contenedorBotones = $("<td>");

            var btnInfo = $("<button class='btn btn-info btn-sm' title='VER LOG'>");
            var btnRemover = $("<button class='btn btn-danger btn-sm' title='EIMINAR'>");

            btnInfo.html('<span class="glyphicon glyphicon-eye-open"></span> ARCHIVO LOG');
            btnRemover.html('<span class="glyphicon glyphicon-remove"></span> ELIMINAR ARCHIVO');

            btnInfo.data("data",datoRegistro);
            btnRemover.data("data",datoRegistro);

            btnInfo.on("click",infoArchivosBodegaPendientes);
            btnRemover.on("click",removerArchivosBodegaPendientes);

            contenedorBotones.append(btnInfo);
            contenedorBotones.append(" ");
            contenedorBotones.append(btnRemover);

            fila.append(contenedorBotones);

            $("#tablaListaArchivosBodegaPendientes tbody").append(fila);

        }

    }

    function infoArchivosBodegaPendientes(){
        var data = $(this).data("data");
        window.open("archivos_sistema/archivos_bodega/"+data.log, '_blank');
    }

    function removerArchivosBodegaPendientes(){
        var data = $(this).data("data");
        alertify.confirm("¿ SEGURO DE ELIMINAR EL ARCHIVO : " + data.nombre +" ?", function (e) {
            if (e) {
                service.procesar("eliminarArchivoBodegaPendiente",data.nombre,mensajeEliminarArchivoBodegaPendiente);
            }
        });
    }

    function mensajeEliminarArchivoBodegaPendiente(evt){
        if(evt > 0){
            service.procesar("listarArchivosBodegaPendientes",cargaListarArchivosBodegaPendientes);
            alertify.success("REGISTRO ELIMINADO CORRECTAMENTE");
        }else{
            alertify.error("REGISTRO NO ELIMINADO");
        }

    }
















    function getLista (array,atributo){
        var nuevoArray = []
        for ( i = 0; i < array.length; i++) {
            valor = array[i][atributo];
            nuevoArray.push(valor);
        }
        return nuevoArray;
    }

    function removeItemFromArr ( arr, item ) {
        var i = arr.indexOf( item );
     
        if ( i !== -1 ) {
            arr.splice( i, 1 );
        }
    }

}


